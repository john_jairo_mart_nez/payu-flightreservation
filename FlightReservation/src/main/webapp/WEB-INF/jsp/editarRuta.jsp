<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>    
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:master>
    <div class="container">
        <div class="row">
            <h4><strong>Editar Ruta</strong></h4>            
            <form:form method="POST" commandName="ruta" cssClass="form-horizontal">
                <form:hidden path="id" />
                <div class="form-group">
                    <label class="control-label col-md-2">Origen</label>
                    <div class="col-md-10">
                        <form:input path="origenId.nombre" id="origenId_nombre" cssClass="form-control"
                                    data-ac-url="origenIdUrl" data-ac-id="origenId_id" data-ac-nombre="origenId_nombre" />
                        <form:errors path="origenId.nombre" cssClass="text-danger" element="span"/>
                        <form:hidden path="origenId.id" id="origenId_id" />
                        <input type="hidden" id="origenIdUrl" value="<spring:url value="/aeropuerto/buscarPorNombre.json" />" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2">Origen</label>
                    <div class="col-md-10">
                        <form:input path="destinoId.nombre" id="destinoId_nombre" cssClass="form-control"
                                    data-ac-url="destinoIdUrl" data-ac-id="destinoId_id" data-ac-nombre="destinoId_nombre" />
                        <form:errors path="destinoId.nombre" cssClass="text-danger" element="span"/>
                        <form:hidden path="destinoId.id" id="destinoId_id" />
                        <input type="hidden" id="destinoIdUrl" value="<spring:url value="/aeropuerto/buscarPorNombre.json" />" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2">Duración</label>
                    <div class="col-md-10">
                        <form:input path="duracion" cssClass="form-control"/>
                        <form:errors path="duracion" cssClass="text-danger" element="span"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10 col-md-offset-2">
                        <button type="submit" class="btn btn-primary">
                            <span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Guardar
                        </button>
                    </div>
                </div>
            </form:form>
            <a href="<c:url value="/ruta/listar.html"/>">Regresar</a>
        </div>
    </div>
</t:master>