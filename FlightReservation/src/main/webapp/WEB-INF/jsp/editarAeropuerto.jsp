<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>    
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:master>
    <div class="container">
        <div class="row">
            <h4><strong>Editar Aeropuerto</strong></h4>            
            <form:form method="POST" commandName="aeropuerto" cssClass="form-horizontal">
                <form:hidden path="id" />
                <div class="form-group">
                    <label for="a" class="control-label col-md-2">Aeropuerto</label>
                    <div class="col-md-10">
                        <form:input path="nombre" cssClass="form-control"/>
                        <form:errors path="nombre" cssClass="text-danger" element="span"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="a" class="control-label col-md-2">Ciudad</label>
                    <div class="col-md-10">
                        <form:input path="ciudadId.nombre" id="ciudadId_nombre" cssClass="form-control" 
                                    data-ac-url="ciudadIdUrl" data-ac-id="ciudadId_id" data-ac-nombre="ciudadId_nombre" />
                        <form:errors path="ciudadId.nombre" cssClass="text-danger" element="span"/>
                        <form:hidden path="ciudadId.id" id="ciudadId_id" />
                        <input type="hidden" id="ciudadIdUrl" value="<spring:url value="/ciudad/buscarPorNombre.json" />" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10 col-md-offset-2">
                        <button type="submit" class="btn btn-primary">
                            <span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Guardar
                        </button>
                    </div>
                </div>
            </form:form>
            <a href="<c:url value="/aeropuerto/listar.html"/>">Regresar</a>
        </div>
    </div>
</t:master>