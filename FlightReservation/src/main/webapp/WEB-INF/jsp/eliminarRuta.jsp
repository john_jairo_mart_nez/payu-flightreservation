<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>    
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:master>
    <div class="container">
        <div class="row">
            <h4><strong>Eliminar Ruta</strong></h4>            
            <h5 class="text-muted">Está seguro que desea eliminar la ruta?</h5>
            <form:form method="POST" commandName="ruta">
                <div class="form-horizontal">
                    <form:hidden path="id" />
                    <div class="form-group">
                        <label class="control-label col-md-2">Origen</label>
                        <div class="col-md-10">
                            ${ruta.origenId.nombre}
                            <form:hidden path="origenId.id" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2">Destino</label>
                        <div class="col-md-10">
                            ${ruta.destinoId.nombre}
                            <form:hidden path="destinoId.id" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2">Duración</label>
                        <div class="col-md-10">
                            ${ruta.duracion}
                            <form:hidden path="duracion" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-10 col-md-offset-2">
                            <button type="submit" class="btn btn-primary">
                                <span class="glyphicon glyphicon-remove"></span>&nbsp;Eliminar
                            </button>
                        </div>
                    </div>
                </div>
                
            </form:form>
        </div>
        <div class="row">
            <a href="<c:url value="/modelo/listar.html"/>">Regresar</a>
        </div>
    </div>
</t:master>