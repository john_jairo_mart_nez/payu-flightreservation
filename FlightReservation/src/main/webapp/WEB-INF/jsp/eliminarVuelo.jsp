<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>    
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<t:master>
    <div class="container">
        <div class="row">
            <h4><strong>Eliminar Vuelo</strong></h4>            
            <h5 class="text-muted">Está seguro que desea eliminar el vuelo?</h5>
            <form:form method="POST" commandName="vuelo">
                <div class="form-horizontal">
                    <form:hidden path="id" />
                    <div class="form-group">
                        <label class="control-label col-md-2">Fecha y Hora de Inicio</label>
                        <div class="col-md-10">
                            ${vuelo.fechaInicio}
                            <input type="hidden" name="fechaInicio" value="<fmt:formatDate value="<%=new Date()%>" pattern="yyyy/MM/dd HH:mm"/>" />
                        </div>
                    </div>

                    <input type="hidden" name="fechaTerminacion" value="<fmt:formatDate value="<%=new Date()%>" pattern="yyyy/MM/dd HH:mm"/>" />

                    <div class="form-group">
                        <label class="control-label col-md-2">Ruta</label>
                        <div class="col-md-10">
                            ${vuelo.rutaId.origenId.nombre} - ${vuelo.rutaId.destinoId.nombre}
                            <form:hidden path="rutaId.id" id="rutaId_id" />
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-md-2">Avion</label>
                        <div class="col-md-10">
                            ${vuelo.avionId.nombre}
                            <form:hidden path="avionId.id" id="avionId_id" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-10 col-md-offset-2">
                            <button type="submit" class="btn btn-primary">
                                <span class="glyphicon glyphicon-remove"></span>&nbsp;Eliminar
                            </button>
                        </div>
                    </div>
                </div>
                
            </form:form>
        </div>
        <div class="row">
            <a href="<c:url value="/vuelo/listar.html"/>">Regresar</a>
        </div>
    </div>
</t:master>