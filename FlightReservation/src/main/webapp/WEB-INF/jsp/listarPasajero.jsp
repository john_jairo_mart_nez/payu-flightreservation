<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:master>
    <div class="container">
        <div class="row">
            <h4 class="text-muted"><strong>Pasajeros</strong></h4>
            <a href="<c:url value="/pasajero/crear.html"/>" class="btn btn-primary">
                <span class="glyphicon glyphicon-plus"></span>Crear Pasajero</a>

            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Identificación</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${pasajeros}" var="pasajero">
                            <tr>
                                <td>${pasajero.nombre}</td>
                                <td>${pasajero.numeroIdentificacion}</td>
                                <td>
                                    <a href="<c:url value="/pasajero/editar.html?id=${pasajero.id}"/>" class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-pencil"></span>&nbsp;Editar</a>
                                </td>
                                <td>
                                    <a href="<c:url value="/pasajero/eliminar.html?id=${pasajero.id}"/>" class="btn btn-sm btn-warning"><span class="glyphicon glyphicon-remove"></span>&nbsp;Eliminar</a>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</t:master>