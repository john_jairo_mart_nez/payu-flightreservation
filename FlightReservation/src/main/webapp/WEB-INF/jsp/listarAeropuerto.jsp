<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:master>
    <div class="container">
        <div class="row">
            <h4 class="text-muted"><strong>Aeropuertos</strong></h4>
            <a href="<c:url value="/aeropuerto/crear.html"/>" class="btn btn-primary">
                <span class="glyphicon glyphicon-plus"></span>Crear Aeropuerto</a>

            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Aeropuerto</th>
                            <th>Ciudad</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${aeropuertos}" var="aeropuerto">
                            <tr>
                                <td>${aeropuerto.nombre}</td>
                                <td>${aeropuerto.ciudadId.nombre}</td>
                                <td>
                                    <a href="<c:url value="/aeropuerto/editar.html?id=${aeropuerto.id}"/>" class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-pencil"></span>&nbsp;Editar</a>
                                </td>
                                <td>
                                    <a href="<c:url value="/aeropuerto/eliminar.html?id=${aeropuerto.id}"/>" class="btn btn-sm btn-warning"><span class="glyphicon glyphicon-remove"></span>&nbsp;Eliminar</a>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</t:master>
