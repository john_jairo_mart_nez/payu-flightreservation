<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>    
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<t:master>
    <div class="container">
        <div class="row">
            <h4><strong>Crear Vuelo</strong></h4>            
            <form:form method="POST" commandName="vuelo">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-md-2">Fecha y Hora de Inicio</label>
                        <div class="col-md-10">
                            <div class='input-group date' data-datetimepicker="true">
                                <input type='text' class="form-control" name="fechaInicio" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                            <form:errors path="fechaInicio" cssClass="text-danger" element="span"/>
                        </div>
                    </div>

                    <input type="hidden" name="fechaTerminacion" value="<fmt:formatDate value="<%=new Date()%>" pattern="yyyy/MM/dd HH:mm"/>" />

                    <div class="form-group">
                        <label class="control-label col-md-2">Ruta</label>
                        <div class="col-md-10">
                            <input type="text" id="rutaId_nombre" class="form-control"
                                        data-ac-url="rutaIdUrl" data-ac-id="rutaId_id" data-ac-nombre="rutaId_nombre" />
                            <form:hidden path="rutaId.id" id="rutaId_id" />
                            <input type="hidden" id="rutaIdUrl" value="<spring:url value="/ruta/buscarPorNombre.json" />" />
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-md-2">Avion</label>
                        <div class="col-md-10">
                            <form:input path="avionId.nombre" id="avionId_nombre" cssClass="form-control"
                                    data-ac-url="avionIdUrl" data-ac-id="avionId_id" data-ac-nombre="avionId_nombre" />
                            <form:errors path="avionId.id" cssClass="text-danger" element="span"/>
                            <form:hidden path="avionId.id" id="avionId_id" />
                            <input type="hidden" id="avionIdUrl" value="<spring:url value="/avion/buscarPorNombre.json" />" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-10 col-md-offset-2">
                            <button type="submit" class="btn btn-primary">
                                <span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Guardar
                            </button>
                        </div>
                    </div>
                </div>
                
            </form:form>
            <a href="<c:url value="/vuelo/listar.html"/>">Regresar</a>
        </div>
    </div>
</t:master>