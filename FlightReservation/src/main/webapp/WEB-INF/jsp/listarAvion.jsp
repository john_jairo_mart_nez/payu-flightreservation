<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:master>
    <div class="container">
        <div class="row">
            <h4 class="text-muted"><strong>Aviones</strong></h4>
            <a href="<c:url value="/avion/crear.html"/>" class="btn btn-primary">
                <span class="glyphicon glyphicon-plus"></span>Crear Avion</a>

            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Avion</th>
                            <th>Modelo</th>
                            <th>Fabricante</th>
                            <th>Capacidad</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${aviones}" var="avion">
                            <tr>
                                <td>${avion.nombre}</td>
                                <td>${avion.modeloId.nombre}</td>
                                <td>${avion.fabricanteId.nombre}</td>
                                <td>${avion.capacidad}</td>
                                <td>
                                    <a href="<c:url value="/avion/editar.html?id=${avion.id}"/>" class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-pencil"></span>&nbsp;Editar</a>
                                </td>
                                <td>
                                    <a href="<c:url value="/avion/eliminar.html?id=${avion.id}"/>" class="btn btn-sm btn-warning"><span class="glyphicon glyphicon-remove"></span>&nbsp;Eliminar</a>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</t:master>
