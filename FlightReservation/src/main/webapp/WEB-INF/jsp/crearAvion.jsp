<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>    
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:master>
    <div class="container">
        <div class="row">
            <h4><strong>Crear Avion</strong></h4>            
            <form:form method="POST" commandName="avion" cssClass="form-horizontal">
                <div class="form-group">
                    <label class="control-label col-md-2">Avion</label>
                    <div class="col-md-10">
                        <form:input path="nombre" cssClass="form-control"/>
                        <form:errors path="nombre" cssClass="text-danger" element="span"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2">Modelo</label>
                    <div class="col-md-10">
                        <form:input path="modeloId.nombre" id="modeloId_nombre" cssClass="form-control" 
                                    data-ac-url="modeloIdUrl" data-ac-id="modeloId_id" data-ac-nombre="modeloId_nombre" />
                        <form:errors path="modeloId.nombre" cssClass="text-danger" element="span"/>
                        <form:hidden path="modeloId.id" id="modeloId_id" />
                        <input type="hidden" id="modeloIdUrl" value="<spring:url value="/modelo/buscarPorNombre.json" />" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2">Fabricante</label>
                    <div class="col-md-10">
                        <form:input path="fabricanteId.nombre" id="fabricanteId_nombre" cssClass="form-control" 
                                    data-ac-url="fabricanteIdUrl" data-ac-id="fabricanteId_id" data-ac-nombre="fabricanteId_nombre" />
                        <form:errors path="fabricanteId.nombre" cssClass="text-danger" element="span"/>
                        <form:hidden path="fabricanteId.id" id="fabricanteId_id" />
                        <input type="hidden" id="fabricanteIdUrl" value="<spring:url value="/fabricante/buscarPorNombre.json" />" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2">Capacidad</label>
                    <div class="col-md-10">
                        <form:input path="capacidad" cssClass="form-control"/>
                        <form:errors path="capacidad" cssClass="text-danger" element="span"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10 col-md-offset-2">
                        <button type="submit" class="btn btn-primary">
                            <span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Guardar
                        </button>
                    </div>
                </div>
            </form:form>
            <a href="<c:url value="/avion/listar.html"/>">Regresar</a>
        </div>
    </div>
</t:master>