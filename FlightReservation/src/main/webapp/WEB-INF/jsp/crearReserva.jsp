<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>    
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<t:master>
    <div class="container">
        <div class="row">
            <h4><strong>Crear Reserva</strong></h4>            
            <form:form method="POST" action="crear.html" commandName="reserva">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-md-2">Vuelo</label>
                        <div class="col-md-10">
                            ${vuelo.rutaId.origenId.nombre} - ${vuelo.rutaId.destinoId.nombre} : ${vuelo.fechaInicio}
                            <form:hidden path="vueloId.id" value="${vuelo.id}" />
                            <form:errors path="vueloId.id" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2">Pasajero</label>
                        <div class="col-md-10">
                            <form:input path="pasajeroId.nombre" cssClass="form-control" id="pasajeroId_nombre" 
                                        data-ac-url="pasajeroIdUrl" data-ac-id="pasajeroId_id" data-ac-nombre="pasajeroId_nombre" />
                            <form:hidden path="pasajeroId.id" id="pasajeroId_id" />
                            <input type="hidden" id="pasajeroIdUrl" value="<spring:url value="/pasajero/buscarPorNombre.json" />" />
                            <form:errors path="pasajeroId.id" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-10 col-md-offset-2">
                            <button type="submit" class="btn btn-primary">
                                <span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Guardar
                            </button>
                        </div>
                    </div>
                </div>
                
            </form:form>
            <a href="<c:url value="/vuelo/listar.html"/>">Regresar</a>
        </div>
    </div>
</t:master>