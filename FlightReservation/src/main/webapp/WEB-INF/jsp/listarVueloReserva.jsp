<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:master>
    <div class="container">
        <div class="row">
            <h4 class="text-muted"><strong>Vuelos para reservar</strong></h4>

            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Origen</th>
                            <th>Destino</th>
                            <th>Avion</th>
                            <th>Fecha y Hora Salida</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${vuelos}" var="vuelo">
                            <tr>
                                <td>${vuelo.rutaId.origenId.nombre}</td>
                                <td>${vuelo.rutaId.destinoId.nombre}</td>
                                <td>${vuelo.avionId.nombre}</td>
                                <td>${vuelo.fechaInicio}</td>
                                <td>
                                    <a href="<c:url value="/reserva/crear.html?vueloId=${vuelo.id}"/>" class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;Reservar</a>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <a href="<c:url value="/reserva/buscarVuelo.html"/>">Regresar</a>
        </div>
    </div>
</t:master>