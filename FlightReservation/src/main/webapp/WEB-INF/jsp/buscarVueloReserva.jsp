<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>    
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<t:master>
    <div class="container">
        <div class="row">
            <h4>Buscar Vuelos Para Reservar</h4>
            <form:form action="listarVuelo.html" method="POST" commandName="buscarVuelo">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-md-2">Ciudad Origen</label>
                        <div class="col-md-10">
                            <form:input path="origenNombre" id="origenNombre" cssClass="form-control" 
                                    data-ac-url="origenIdUrl" data-ac-id="origenId" data-ac-nombre="origenNombre" />
                            <form:errors path="origenNombre" cssClass="text-danger" element="span"/>
                            <form:hidden path="origenId" id="origenId" />
                            <input type="hidden" id="origenIdUrl" value="<spring:url value="/ciudad/buscarPorNombre.json" />" />
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-md-2">Ciudad Destino</label>
                        <div class="col-md-10">
                            <form:input path="destinoNombre" id="destinoNombre" cssClass="form-control" 
                                    data-ac-url="destinoIdUrl" data-ac-id="destinoId" data-ac-nombre="destinoNombre" />
                            <form:errors path="destinoNombre" cssClass="text-danger" element="span"/>
                            <form:hidden path="destinoId" id="destinoId" />
                            <input type="hidden" id="destinoIdUrl" value="<spring:url value="/ciudad/buscarPorNombre.json" />" />
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-md-2">Fecha</label>
                        <div class="col-md-10">
                            <form:input path="fecha" cssClass="form-control date-picker"/>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-md-10 col-md-offset-2">
                            <button type="submit" class="btn btn-primary">
                                <span class="glyphicon glyphicon-search"></span>&nbsp;Buscar
                            </button>
                        </div>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</t:master>