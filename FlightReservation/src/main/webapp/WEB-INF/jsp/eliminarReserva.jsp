<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>    
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<t:master>
    <div class="container">
        <div class="row">
            <h4><strong>Eliminar Reserva</strong></h4>            
            <h5 class="text-muted">Está seguro que desea eliminar la reserva?</h5>
            <form:form method="POST" commandName="reserva" cssClass="form-horizontal">
                <div class="form-horizontal">
                    <form:hidden path="id" />
                    <div class="form-group">
                        <label class="control-label col-md-2">Vuelo</label>
                        <div class="col-md-10">
                            ${reserva.vueloId.rutaId.origenId.nombre} - ${reserva.vueloId.rutaId.destinoId.nombre} : ${reserva.vueloId.fechaInicio}
                            <form:hidden path="vueloId.id" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2">Pasajero</label>
                        <div class="col-md-10">
                            ${reserva.pasajeroId.nombre}
                            <form:hidden path="pasajeroId.id"/>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-md-10 col-md-offset-2">
                            <button type="submit" class="btn btn-primary">
                                <span class="glyphicon glyphicon-remove"></span>&nbsp;Eliminar
                            </button>
                        </div>
                    </div>
                </div>
                
            </form:form>
        </div>
        <div class="row">
            <a href="<c:url value="/vuelo/listar.html"/>">Regresar</a>
        </div>
    </div>
</t:master>