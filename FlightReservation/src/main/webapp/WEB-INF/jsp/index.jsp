<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:master>
    <div class="row">
        <div class="col-md-4 text-center">
            <img class="img-circle" src="<c:url value="/assets/images/avion.png"/>" alt="Avion">
            <h3 class="text-center">Prueba Práctica PayU</h3>
            <p>Utilizando tecnologías Java... </p>
            <a href="#" class="btn btn-default">Conocer más</a>
        </div>
            
        <div class="col-md-4 text-center">
            <img class="img-circle" src="<c:url value="/assets/images/avion.png"/>" alt="Avion">
            <h3 class="text-center">Prueba Práctica PayU</h3>
            <p>Por: John Jairo Martínez</p>
            <a href="#" class="btn btn-default">Conocer más</a>
        </div>
            
        <div class="col-md-4 text-center">
            <img class="img-circle" src="<c:url value="/assets/images/avion.png"/>" alt="Avion">
            <h3 class="text-center">Prueba Práctica PayU</h3>
            <p>SpringMVC, JPA, Spring Data...</p>
            <a href="#" class="btn btn-default">Conocer más</a>
        </div>
        
    </div>
</t:master>
