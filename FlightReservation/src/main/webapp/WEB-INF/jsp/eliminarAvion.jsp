<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>    
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:master>
    <div class="container">
        <div class="row">
            <h4><strong>Eliminar Avion</strong></h4>            
            <h5 class="text-muted">Está seguro que desea eliminar el avion?</h5>
            <form:form method="POST" commandName="avion">
                <div class="form-horizontal">
                    <form:hidden path="id" />
                    <div class="form-group">
                        <label class="control-label col-md-2">Avion</label>
                        <div class="col-md-10">
                            ${avion.nombre}
                            <form:hidden path="nombre" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2">Modelo</label>
                        <div class="col-md-10">
                            ${avion.modeloId.nombre}
                            <form:hidden path="modeloId.nombre" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2">Fabricante</label>
                        <div class="col-md-10">
                            ${avion.fabricanteId.nombre}
                            <form:hidden path="fabricanteId.nombre" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2">Capacidad</label>
                        <div class="col-md-10">
                            ${avion.capacidad}
                            <form:hidden path="capacidad" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-10 col-md-offset-2">
                            <button type="submit" class="btn btn-primary">
                                <span class="glyphicon glyphicon-remove"></span>&nbsp;Eliminar
                            </button>
                        </div>
                    </div>
                </div>
            </form:form>
        </div>
        <div class="row">
            <a href="<c:url value="/avion/listar.html"/>">Regresar</a>
        </div>
    </div>
</t:master>