<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:master>
    <div class="container">
        <c:if test="${msgError != null && !msgError.isEmpty()}">
            <div class="row">
                <div class="alert alert-danger" role="alert">
                    ${msgError}
                </div>
            </div>
        </c:if>
        <div class="row">
            <h4 class="text-muted"><strong>Reservas</strong></h4>
            <a href="<c:url value="/reserva/buscarVuelo.html"/>" class="btn btn-primary">
                <span class="glyphicon glyphicon-plus"></span>Crear Reserva</a>

            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Pasajero</th>
                            <th>Origen</th>
                            <th>Destino</th>
                            <th>Fecha y Hora Salida</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${reservas}" var="reserva">
                            <tr>
                                <td>${reserva.pasajeroId.nombre}</td>
                                <td>${reserva.vueloId.rutaId.origenId.nombre}</td>
                                <td>${reserva.vueloId.rutaId.destinoId.nombre}</td>
                                <td>${reserva.vueloId.fechaInicio}</td>
                                <td>
                                    <a href="<c:url value="/reserva/editar.html?id=${reserva.id}"/>" class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-pencil"></span>&nbsp;Editar</a>
                                </td>
                                <td>
                                    <a href="<c:url value="/reserva/eliminar.html?id=${reserva.id}"/>" class="btn btn-sm btn-warning"><span class="glyphicon glyphicon-remove"></span>&nbsp;Eliminar</a>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</t:master>