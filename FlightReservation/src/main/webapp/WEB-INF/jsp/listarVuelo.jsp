<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:master>
    <div class="container">
        <c:if test="${msgError != null && !msgError.isEmpty()}">
            <div class="row">
                <div class="alert alert-danger" role="alert">
                    ${msgError}
                </div>
            </div>
        </c:if>
        <div class="row">
            <h4 class="text-muted"><strong>Vuelos</strong></h4>
            <a href="<c:url value="/vuelo/crear.html"/>" class="btn btn-primary">
                <span class="glyphicon glyphicon-plus"></span>Crear Vuelo</a>

            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Origen</th>
                            <th>Destino</th>
                            <th>Avion</th>
                            <th>Fecha y Hora Salida</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${vuelos}" var="vuelo">
                            <tr>
                                <td>${vuelo.rutaId.origenId.nombre}</td>
                                <td>${vuelo.rutaId.destinoId.nombre}</td>
                                <td>${vuelo.avionId.nombre}</td>
                                <td>${vuelo.fechaInicio}</td>
                                <td>
                                    <a href="<c:url value="/vuelo/editar.html?id=${vuelo.id}"/>" class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-pencil"></span>&nbsp;Editar</a>
                                </td>
                                <td>
                                    <a href="<c:url value="/vuelo/eliminar.html?id=${vuelo.id}"/>" class="btn btn-sm btn-warning"><span class="glyphicon glyphicon-remove"></span>&nbsp;Eliminar</a>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</t:master>