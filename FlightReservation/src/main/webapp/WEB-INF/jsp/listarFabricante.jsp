<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:master>
    <div class="container">
        <div class="row">
            <h4 class="text-muted"><strong>Fabricantes</strong></h4>
            <a href="<c:url value="/fabricante/crear.html"/>" class="btn btn-primary">
                <span class="glyphicon glyphicon-plus"></span>Crear Fabricante</a>

            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Fabricante</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${fabricantes}" var="fabricante">
                            <tr>
                                <td>${fabricante.nombre}</td>
                                <td>
                                    <a href="<c:url value="/fabricante/editar.html?id=${fabricante.id}"/>" class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-pencil"></span>&nbsp;Editar</a>
                                </td>
                                <td>
                                    <a href="<c:url value="/fabricante/eliminar.html?id=${fabricante.id}"/>" class="btn btn-sm btn-warning"><span class="glyphicon glyphicon-remove"></span>&nbsp;Eliminar</a>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</t:master>