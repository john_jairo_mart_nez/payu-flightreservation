<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>    
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:master>
    <div class="container">
        <div class="row">
            <h4><strong>Eliminar Pasajero</strong></h4>            
            <h5 class="text-muted">Está seguro que desea eliminar el pasajero?</h5>
            <form:form method="POST" commandName="pasajero">
                <div class="form-horizontal">
                    <form:hidden path="id" />
                    <div class="form-group">
                        <label for="a" class="control-label col-md-2">Nombre</label>
                        <div class="col-md-10">
                            ${pasajero.nombre}
                            <form:hidden path="nombre" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="a" class="control-label col-md-2">Identificación</label>
                        <div class="col-md-10">
                            ${pasajero.numeroIdentificacion}
                            <form:hidden path="numeroIdentificacion" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-10 col-md-offset-2">
                            <button type="submit" class="btn btn-primary">
                                <span class="glyphicon glyphicon-remove"></span>&nbsp;Eliminar
                            </button>
                        </div>
                    </div>
                </div>
            </form:form>
        </div>
        <div class="row">
            <a href="<c:url value="/pasajero/listar.html"/>">Regresar</a>
        </div>
    </div>
</t:master>