<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>    
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<t:master>
    <div class="container">
        <div class="row">
            <h4>Generar Informe</h4>
            <form:form method="POST" commandName="informe">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-md-2">Fecha Inicial</label>
                        <div class="col-md-10">
                            <form:input path="fechaInicial" cssClass="form-control date-picker"/>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-md-2">Fecha Final</label>
                        <div class="col-md-10">
                            <form:input path="fechaFinal" cssClass="form-control date-picker"/>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-md-2">Avion</label>
                        <div class="col-md-10">
                            <input type="text" id="avionId_nombre" class="form-control"
                                    data-ac-url="avionIdUrl" data-ac-id="avionId_id" data-ac-nombre="avionId_nombre" />
                            <form:errors path="avionId" cssClass="text-danger" element="span"/>
                            <form:hidden path="avionId" id="avionId_id" />
                            <input type="hidden" id="avionIdUrl" value="<spring:url value="/avion/buscarPorNombre.json" />" />
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-md-10 col-md-offset-2">
                            <button type="submit" class="btn btn-primary">
                                <span class="glyphicon glyphicon-search"></span>&nbsp;Buscar
                            </button>
                        </div>
                    </div>
                </div>
            </form:form>
        </div>
        <div class="row">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Origen</th>
                            <th>Destino</th>
                            <th>Número Vuelos</th>
                            <th>Número Pasajeros</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${resultados}" var="resultado">
                            <tr>
                                <td>${resultado.origen}</td>
                                <td>${resultado.destino}</td>
                                <td>${resultado.numeroVuelos}</td>
                                <td>${resultado.numeroPasajeros}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</t:master>