<%@tag description="master" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>JSP Page</title>
        <link href="<c:url value="/assets/css/bootstrap.min.css"/>" rel="stylesheet" />
        <link href="<c:url value="/assets/css/bootstrap-datetimepicker.css"/>" rel="stylesheet" />
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
    </head>
    <body>
        
        <div>
            
            <nav class="navbar navbar-inverse">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">Reserva Vuelos</a>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav">
                            <li><a href="<c:url value="/" />">Inicio</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Rutas Aereas <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="<c:url value="/ciudad/listar.html"/>">Ciudad</a></li>
                                    <li><a href="<c:url value="/aeropuerto/listar.html"/>">Aeropuerto</a></li>
                                    <li><a href="<c:url value="/ruta/listar.html"/>">Ruta</a></li>
                                    <li><a href="<c:url value="/vuelo/listar.html"/>">Vuelos</a></li>
                                </ul>
                            </li>
                            
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Aviones <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="<c:url value="/fabricante/listar.html"/>">Fabricantes</a></li>
                                    <li><a href="<c:url value="/modelo/listar.html"/>">Modelos</a></li>
                                    <li><a href="<c:url value="/avion/listar.html"/>">Aviones</a></li>
                                </ul>
                            </li>
                            
                            <li><a href="<c:url value="/pasajero/listar.html"/>">Pasajeros</a></li>
                            
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reservas <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="<c:url value="/reserva/buscarVuelo.html"/>">Crear</a></li>
                                    <li><a href="<c:url value="/reserva/listar.html"/>">Ver todas</a></li>
                                </ul>
                            </li>
                            
                            <li><a href="<c:url value="/informe/buscarInforme.html"/>">Informe</a></li>
                            
                        </ul>
                    </div><!--/.nav-collapse -->
                </div><!--/.container-fluid -->
            </nav>
            
        </div>
        <div class="container-fluid">
            <jsp:doBody/>
        </div>
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="<c:url value="/assets/js/moment-with-locales.js"/>"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
        <script src="<c:url value="/assets/js/jquery-ui-datepicker-es.js"/>"></script>
        <script src="<c:url value="/assets/js/bootstrap.min.js"/>"></script>
        <script src="<c:url value="/assets/js/bootstrap-datetimepicker.js"/>"></script>
        <script src="<c:url value="/assets/js/sitio.js"/>"></script>
    </body>
</html>
