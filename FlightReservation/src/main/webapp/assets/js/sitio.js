$(function () {
    // autocomplete
    var elementosAutocomplete = $('input[data-ac-url]');
    if (elementosAutocomplete) {
        $.each(elementosAutocomplete, function (indice, elemento) {
            var urlElemento = $(elemento).attr('data-ac-url');
            var urlValor = $('#'+urlElemento).val();
            var idElemento = $(elemento).attr('data-ac-id');
            var nombreElemento = $(elemento).attr('data-ac-nombre');
            $(elemento).autocomplete({
                minLength: 2,
                focus: function (event, ui) {
                    // prevent autocomplete from updating the textbox
                    event.preventDefault();
                },
                source: function (request, response) {
                    // hacer el llamado ajax
                    $.ajax({
                        type: 'POST',
                        datatype: 'json',
                        url: urlValor,
                        data: {nombre: request.term},
                        success: function (data) {
                            response($.map(data, function (item) {
                                return {
                                    label: item.nombre,
                                    value: item.nombre,
                                    id: item.id,
                                    nombre: item.nombre
                                };
                            }));
                        }, 
                        error: function(v1, v2, v3){
                            alert('error');
                        }
                    });
                },
                select: function (event, ui) {
                    // convertir la seleccion a los dos controles
                    event.preventDefault();
                    $('#' + idElemento).val(ui.item.id);
                    $('#' + nombreElemento).val(ui.item.nombre);
                }
            });
        });
    }
    // datetimepicker
    $('div[data-datetimepicker]').datetimepicker({
        format: 'YYYY/MM/DD HH:mm'
    });
    // datepicker
    var esDateFormat = 'yy/mm/dd';
    $(".date-picker").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: esDateFormat,
        regional: "es"
    });
});