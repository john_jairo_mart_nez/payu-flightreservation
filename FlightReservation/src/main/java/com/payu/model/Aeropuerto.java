
package com.payu.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * @author johnmartinez
 */
@Entity
@Table(name = "aeropuerto", catalog = "flightdb", schema = "public")
public class Aeropuerto implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * identificador
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    /**
     * nombre del aeropuerto
     */
    @Basic(optional = false)
    @NotNull
    @Column(name = "nombre")
    private String nombre;
    
    /**
     * ciudad del aeropuerto
     */
    @JoinColumn(name = "ciudad_id", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Ciudad ciudadId;
    
    /**
     * rutas a las que el aeropuerto sirve como destino
     */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "destinoId", fetch = FetchType.LAZY)
    private List<Ruta> rutasDestino;
    
    /**
     * rutas a las que el aeropuerto sirve como origen
     */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "origenId", fetch = FetchType.LAZY)
    private List<Ruta> rutasOrigen;

    /**
     * Constructor
     */
    public Aeropuerto() {
    }

    /**
     * Constructor
     * @param id identificador
     */
    public Aeropuerto(Integer id) {
        this.id = id;
    }

    /**
     * Constructor
     * @param id identificador 
     * @param nombre nombre
     */
    public Aeropuerto(Integer id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return ciudad del aeropuerto
     */
    public Ciudad getCiudadId() {
        return ciudadId;
    }

    /**
     * @param ciudadId ciudad del aeropuerto
     */
    public void setCiudadId(Ciudad ciudadId) {
        this.ciudadId = ciudadId;
    }

    /**
     * @return rutas destino
     */
    @XmlTransient
    @JsonIgnore
    public List<Ruta> getRutasDestino() {
        return rutasDestino;
    }

    /**
     * @param rutaList to set
     */
    public void setRutasDestino(List<Ruta> rutaList) {
        this.rutasDestino = rutaList;
    }

    /**
     * @return rutas origen
     */
    @XmlTransient
    @JsonIgnore
    public List<Ruta> getRutasOrigen() {
        return rutasOrigen;
    }

    /**
     * @param rutaList1 rutas origen
     */
    public void setRutasOrigen(List<Ruta> rutaList1) {
        this.rutasOrigen = rutaList1;
    }

    /**
     * Toma el hash del id
     * @return 
     */
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    /**
     * compara con base en el id
     * @param object
     * @return 
     */
    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Aeropuerto)) {
            return false;
        }
        Aeropuerto other = (Aeropuerto) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.payu.model.Aeropuerto[ id=" + id + " ]";
    }
    
}
