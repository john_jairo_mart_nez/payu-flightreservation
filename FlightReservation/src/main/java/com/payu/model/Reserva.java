
package com.payu.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author johnmartinez
 */
@Entity
@Table(name = "reserva", catalog = "flightdb", schema = "public")
public class Reserva implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @JoinColumn(name = "vuelo_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Vuelo vueloId;
    @JoinColumn(name = "pasajero_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Pasajero pasajeroId;

    public Reserva() {
    }

    public Reserva(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Vuelo getVueloId() {
        return vueloId;
    }

    public void setVueloId(Vuelo vueloId) {
        this.vueloId = vueloId;
    }

    public Pasajero getPasajeroId() {
        return pasajeroId;
    }

    public void setPasajeroId(Pasajero pasajeroId) {
        this.pasajeroId = pasajeroId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reserva)) {
            return false;
        }
        Reserva other = (Reserva) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.payu.model.Reserva[ id=" + id + " ]";
    }
    
}
