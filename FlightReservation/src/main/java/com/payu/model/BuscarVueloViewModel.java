package com.payu.model;

import java.util.Date;
import javax.validation.constraints.NotNull;

/**
 * Clase que define los parámetros de busqueda de un vuelo
 * @author johnmartinez
 */
public class BuscarVueloViewModel {
    
    /**
     * nombre de la ciudad de origen
     */
    private String origenNombre;
    
    /**
     * identificador de la ciudad de origen
     */
    @NotNull
    private Integer origenId;
    
    /**
     * nombre de la ciudad de destino
     */
    private String destinoNombre;
    
    /**
     * identificador de la ciudad de destino
     */
    @NotNull
    private Integer destinoId;
    
    /**
     * fecha de busqueda del vuelo
     */
    @NotNull
    private Date fecha;

    /**
     * @return the origenNombre
     */
    public String getOrigenNombre() {
        return origenNombre;
    }

    /**
     * @param origenNombre the origenNombre to set
     */
    public void setOrigenNombre(String origenNombre) {
        this.origenNombre = origenNombre;
    }

    /**
     * @return the origenId
     */
    public Integer getOrigenId() {
        return origenId;
    }

    /**
     * @param origenId the origenId to set
     */
    public void setOrigenId(Integer origenId) {
        this.origenId = origenId;
    }

    /**
     * @return the destinoNombre
     */
    public String getDestinoNombre() {
        return destinoNombre;
    }

    /**
     * @param destinoNombre the destinoNombre to set
     */
    public void setDestinoNombre(String destinoNombre) {
        this.destinoNombre = destinoNombre;
    }

    /**
     * @return the destinoId
     */
    public Integer getDestinoId() {
        return destinoId;
    }

    /**
     * @param destinoId the destinoId to set
     */
    public void setDestinoId(Integer destinoId) {
        this.destinoId = destinoId;
    }

    /**
     * @return the fecha
     */
    public Date getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    
    
    
}
