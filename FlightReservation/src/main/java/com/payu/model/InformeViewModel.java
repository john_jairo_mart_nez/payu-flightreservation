
package com.payu.model;

import java.util.Date;

/**
 * Parametros de busqueda para generar un informe
 * @author johnmartinez
 */
public class InformeViewModel {
    
    /**
     * fecha inicial de busqueda
     */
    private Date fechaInicial;
    
    /**
     * fecha final de busqueda
     */
    private Date fechaFinal;
    
    /**
     * identificador del avion
     */
    private Integer avionId;

    /**
     * @return the fechaInicial
     */
    public Date getFechaInicial() {
        return fechaInicial;
    }

    /**
     * @param fechaInicial the fechaInicial to set
     */
    public void setFechaInicial(Date fechaInicial) {
        this.fechaInicial = fechaInicial;
    }

    /**
     * @return the fechaFinal
     */
    public Date getFechaFinal() {
        return fechaFinal;
    }

    /**
     * @param fechaFinal the fechaFinal to set
     */
    public void setFechaFinal(Date fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    /**
     * @return the avionId
     */
    public Integer getAvionId() {
        return avionId;
    }

    /**
     * @param avionId the avionId to set
     */
    public void setAvionId(Integer avionId) {
        this.avionId = avionId;
    }
    
}
