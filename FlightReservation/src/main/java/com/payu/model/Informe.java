package com.payu.model;

/**
 * Define el formato del informe que va a generar el sistema
 * @author johnmartinez
 */
public class Informe {
    
    /**
     * origen de la ruta
     */
    private String origen;
    /**
     * destino de la ruta
     */
    private String destino;
    /**
     * numero de vuelos realizados por el avion
     */
    private Long numeroVuelos;
    /**
     * numero de pasajeros transportados por el avion
     */
    private Long numeroPasajeros;

    public Informe(String origen, String destino, Long numeroVuelos, Long numeroPasajeros) {
        this.origen = origen;
        this.destino = destino;
        this.numeroVuelos = numeroVuelos;
        this.numeroPasajeros = numeroPasajeros;
    }
    
    /**
     * @return the origen
     */
    public String getOrigen() {
        return origen;
    }

    /**
     * @param origen the origen to set
     */
    public void setOrigen(String origen) {
        this.origen = origen;
    }

    /**
     * @return the destino
     */
    public String getDestino() {
        return destino;
    }

    /**
     * @param destino the destino to set
     */
    public void setDestino(String destino) {
        this.destino = destino;
    }

    /**
     * @return the numeroVuelos
     */
    public Long getNumeroVuelos() {
        return numeroVuelos;
    }

    /**
     * @param numeroVuelos the numeroVuelos to set
     */
    public void setNumeroVuelos(Long numeroVuelos) {
        this.numeroVuelos = numeroVuelos;
    }

    /**
     * @return the numeroPasajeros
     */
    public Long getNumeroPasajeros() {
        return numeroPasajeros;
    }

    /**
     * @param numeroPasajeros the numeroPasajeros to set
     */
    public void setNumeroPasajeros(Long numeroPasajeros) {
        this.numeroPasajeros = numeroPasajeros;
    }
    
}
