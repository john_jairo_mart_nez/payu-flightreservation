
package com.payu.model;

/**
 * Parametros a mostrar sobre una ruta en autocompletar
 * @author johnmartinez
 */
public class RutaViewModel {
    
    /**
     * Identificador de la ruta
     */
    private Integer id;
    /**
     * Nombre descriptivo de la ruta
     */
    private String nombre;

    public RutaViewModel(Integer id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }
    
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
}
