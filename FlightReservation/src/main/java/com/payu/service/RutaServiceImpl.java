/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.payu.service;

import com.payu.model.Ruta;
import com.payu.model.RutaViewModel;
import com.payu.repository.RutaRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author johnmartinez
 */
@Service
public class RutaServiceImpl implements RutaService {

    @Autowired
    private RutaRepository rutaRepository;
    
    public List<Ruta> findAll() {
        Sort orden = new Sort(Sort.Direction.ASC, "origenId.id");
        return (List<Ruta>) rutaRepository.findAll(orden);
    }

    public Ruta findById(Integer id) {
        return rutaRepository.findOne(id);
    }

    @Transactional
    public Ruta save(Ruta ruta) {
        ruta = rutaRepository.save(ruta);
        return ruta;
    }

    @Transactional
    public void delete(Ruta ruta) {
        rutaRepository.delete(ruta.getId());
    }

    public List<RutaViewModel> findByNombreLike(String nombre) {
        // TODO: Buscar una manera mas eficiente de hacer la consulta
        if(nombre == null) nombre = "";
        String[] nombres = nombre.split("-");
        String origen = "";
        String destino = "";
        if(nombres.length > 0){
            origen = "%" + nombres[0].trim() + "%";
        }
        if(nombres.length > 1){
            destino = "%" + nombres[1].trim() + "%";
        }
        List<RutaViewModel> rutas = rutaRepository.findByNombreLike("%" + origen + "%", "%" + destino + "%");
        return rutas;
    }
    
}
