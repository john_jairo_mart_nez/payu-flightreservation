/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.payu.service;

import com.payu.model.Modelo;
import java.util.List;

/**
 *
 * @author johnmartinez
 */
public interface ModeloService {
    
    /**
     * Encuentra todas las entidades
     * @return lista de entidades
     */
    List<Modelo> findAll();
    
    /**
     * Encuentra una entidad
     * @param id
     * @return entidad
     */
    Modelo findById(Integer id);
    
    /**
     * Persiste la entidad 
     * @param modelo   a persistir
     * @return entidad persistida
     */
    Modelo save(Modelo modelo);
    
    /**
     * Elimina una entidad
     * @param modelo a eliminar
     */
    void delete(Modelo modelo);

    /**
     * Encuentra una entidad por nombre
     * @param nombre patron a buscar
     * @return lista de entidades
     */
    public List<Modelo> findByNombreLike(String nombre);

    
}
