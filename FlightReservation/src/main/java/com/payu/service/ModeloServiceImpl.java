/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.payu.service;

import com.payu.model.Modelo;
import com.payu.repository.ModeloRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 *
 * @author johnmartinez
 */
@Service
public class ModeloServiceImpl implements ModeloService{

    @Autowired
    private ModeloRepository modeloRepository;
    
    public List<Modelo> findAll() {
        Sort orden = new Sort(Sort.Direction.ASC, "nombre");
        return (List<Modelo>) modeloRepository.findAll(orden);
    }

    public Modelo findById(Integer id) {
        return modeloRepository.findOne(id);
    }

    public Modelo save(Modelo modelo) {
        return modeloRepository.save(modelo);
    }

    public void delete(Modelo modelo) {
        modeloRepository.delete(modelo);
    }

    public List<Modelo> findByNombreLike(String nombre) {
        return modeloRepository.findByNombreLike("%" + nombre + "%");
    }
            
}
