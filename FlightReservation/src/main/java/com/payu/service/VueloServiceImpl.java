/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.payu.service;

import com.payu.model.BuscarVueloViewModel;
import com.payu.model.Ruta;
import com.payu.model.Vuelo;
import com.payu.repository.RutaRepository;
import com.payu.repository.VueloRepository;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author johnmartinez
 */
@Service
public class VueloServiceImpl implements VueloService {
    
    @Autowired
    private VueloRepository vueloRepository;

    @Autowired
    private RutaRepository rutaRepository;
    
    public List<Vuelo> findAll() {
        return (List<Vuelo>) vueloRepository.findAll();
    }

    public Vuelo findById(Integer id) {
        return vueloRepository.findOne(id);
    }

    @Transactional
    public Vuelo save(Vuelo vuelo) {
        // 1- Calcular la fecha de terminación con base en la duracion de la ruta
        Ruta ruta = rutaRepository.findOne(vuelo.getRutaId().getId());
        vuelo.setRutaId(ruta);
        vuelo.calcularFechaTerminacion();
        // 2- Calcular verificar que el avion no esté ocupado
        List<Vuelo> vuelosConflictivos = 
                vueloRepository.findVuelosConflicto(
                    vuelo.getAvionId().getId(), vuelo.getFechaInicio(), vuelo.getFechaTerminacion());
        if(vuelosConflictivos.isEmpty()){
            return vueloRepository.save(vuelo);
        }
        return null;
    }

    @Transactional
    public void delete(Vuelo vuelo) {
        vueloRepository.delete(vuelo.getId());
    }

    public List<Vuelo> findBy(BuscarVueloViewModel buscarVuelo) {
        Calendar c1 = Calendar.getInstance();
        c1.setTime(buscarVuelo.getFecha());
        c1.set(c1.get(Calendar.YEAR), c1.get(Calendar.MONTH), c1.get(Calendar.DATE), 0, 0, 0);
        Calendar c2 = Calendar.getInstance();
        c2.setTime(buscarVuelo.getFecha());
        c2.set(c2.get(Calendar.YEAR), c2.get(Calendar.MONTH), c2.get(Calendar.DATE), 0, 0, 0);
        c2.add(Calendar.DATE, 1);
        List<Vuelo> vuelos = vueloRepository.findByFechaCiudad(c1.getTime(), c2.getTime(), 
                buscarVuelo.getOrigenId(), buscarVuelo.getDestinoId());
        return vuelos;
    }
    
    
    
    
}
