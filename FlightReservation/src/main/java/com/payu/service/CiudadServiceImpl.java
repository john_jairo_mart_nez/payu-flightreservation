/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.payu.service;

import com.payu.model.Ciudad;
import com.payu.repository.CiudadRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author johnmartinez
 */
@Service("ciudadService")
public class CiudadServiceImpl implements CiudadService{

    @Autowired
    private CiudadRepository ciudadRepository;
    
    @Transactional
    public Ciudad save(Ciudad ciudad) {
        ciudad = ciudadRepository.save(ciudad);
        return ciudad;
    }

    public List<Ciudad> findAllCiudades() {
        Sort orden = new Sort(Sort.Direction.ASC, "nombre");
        return (List<Ciudad>) ciudadRepository.findAll(orden);
    }

    public Ciudad findById(Integer id) {
        return ciudadRepository.findOne(id);
    }

    @Transactional
    public void delete(Ciudad ciudad) {
        ciudadRepository.delete(ciudad);
    }
    
    public List<Ciudad> findByNombreLike(String nombre){
        nombre = "%" + nombre + "%";
        List<Ciudad> ciudades = ciudadRepository.findByNombreLike(nombre);
        return ciudades;
    }
    
}
