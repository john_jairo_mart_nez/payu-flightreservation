/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.payu.service;

import com.payu.model.Avion;
import com.payu.model.Informe;
import com.payu.repository.AvionRepository;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author johnmartinez
 */
@Service
public class AvionServiceImpl implements AvionService {

    @Autowired
    private AvionRepository avionRepository;
    
    @Transactional
    public Avion save(Avion avion) {
        avion = avionRepository.save(avion);
        return avion;
    }

    public List<Avion> findAll() {
        Sort orden = new Sort(Sort.Direction.ASC, "nombre");
        return (List<Avion>) avionRepository.findAll(orden);
    }

    public Avion findById(Integer id) {
        return avionRepository.findOne(id);
    }

    public void delete(Avion avion) {
        avionRepository.delete(avion.getId());
    }

    public List<Avion> findByNombreLike(String nombre) {
        return avionRepository.findByNombreLike("%" + nombre + "%");
    }
    
    public List<Informe> generateReport(Date fechaInicio, Date fechaFinal, Integer avionId){
        return avionRepository.findReport(fechaInicio, fechaFinal, avionId);
    }
    
}
