/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.payu.service;

import com.payu.model.Reserva;
import com.payu.repository.ReservaRepository;
import com.payu.repository.VueloRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author johnmartinez
 */
@Service
public class ReservaServiceImpl implements ReservaSevice{

    @Autowired
    private ReservaRepository reservaRepository;
    
    @Autowired
    private VueloRepository vueloRepository;
    
    public List<Reserva> findAll() {
        return (List<Reserva>) reservaRepository.findAll();
    }

    public Reserva findById(Integer id) {
        return reservaRepository.findOne(id);
    }

    public Reserva save(Reserva reserva) {
        // 1- Verificar que la persona no tenga un registro para ese vuelo (en insert)
        if(reserva.getId() == null){
            List<Reserva> reservasPersona = reservaRepository.FindBy(reserva.getPasajeroId().getId(), reserva.getVueloId().getId());
            if(!reservasPersona.isEmpty()){
                return null;
            }
        }
        // 2- Verificar que el vuelo aún tenga capacidad
        long capacidad = vueloRepository.findCapacidadActual(reserva.getVueloId().getId());
        if(capacidad <= 0) {
            return null;
        }
        // guardar reserva
        reserva = reservaRepository.save(reserva);
        return reserva;
    }

    public void delete(Reserva reserva) {
        reservaRepository.delete(reserva.getId());
    }
    
}
