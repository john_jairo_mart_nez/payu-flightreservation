
package com.payu.service;

import com.payu.model.Ruta;
import com.payu.model.RutaViewModel;
import java.util.List;

/**
 * Servicio que maneja las operaciones sobre la entidad ruta
 * @author johnmartinez
 */
public interface RutaService {
    
    /**
     * Encuentra todas las entidades
     * @return lista de entidades
     */
    List<Ruta> findAll();
    
    /**
     * Encuentra una entidad
     * @param id
     * @return entidad
     */
    Ruta findById(Integer id);
    
    /**
     * Persiste la entidad 
     * @param ruta a persistir
     * @return entidad persistida
     */
    Ruta save(Ruta ruta);
    
    /**
     * Elimina una entidad
     * @param ruta a eliminar
     */
    void delete(Ruta ruta);

    /**
     * Encuentra una entidad por nombre
     * @param nombre patron a buscar
     * @return lista de entidades
     */
    List<RutaViewModel> findByNombreLike(String nombre);
}
