/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.payu.service;

import com.payu.model.Aeropuerto;
import com.payu.repository.AeropuertoRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author johnmartinez
 */
@Service
public class AeropuertoServiceImpl implements AeropuertoService {

    @Autowired
    private AeropuertoRepository aeropuertoRepository;
    
    @Transactional
    public Aeropuerto save(Aeropuerto aeropuerto) {
        aeropuerto = aeropuertoRepository.save(aeropuerto);
        return aeropuerto;
    }

    public List<Aeropuerto> findAll() {
        Sort orden = new Sort(Sort.Direction.ASC, "nombre");
        return (List<Aeropuerto>) aeropuertoRepository.findAll(orden);
    }

    public Aeropuerto findById(Integer id) {
        return aeropuertoRepository.findOne(id);
    }

    @Transactional
    public void delete(Aeropuerto aeropuerto) {
        aeropuertoRepository.delete(aeropuerto.getId());
    }

    public List<Aeropuerto> findByNombreLike(String nombre) {
        return aeropuertoRepository.findByNombreLike("%" + nombre + "%");
    }
    
    
}
