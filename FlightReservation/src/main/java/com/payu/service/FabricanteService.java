package com.payu.service;

import com.payu.model.Fabricante;
import java.util.List;

/**
 * Servicio que maneja las operaciones sobre la entidad fabricante
 * @author johnmartinez
 */
public interface FabricanteService {
    
    /**
     * Encuentra todas las entidades
     * @return lista de entidades
     */
    List<Fabricante> findAll();
    
    /**
     * Encuentra una entidad
     * @param id
     * @return entidad
     */
    Fabricante findById(Integer id);
    
    /**
     * Persiste la entidad 
     * @param fabricante   a persistir
     * @return entidad persistida
     */
    Fabricante save(Fabricante fabricante);
    
    /**
     * Elimina una entidad
     * @param fabricante   a eliminar
     */
    void delete(Fabricante fabricante);

    /**
     * Encuentra una entidad por nombre
     * @param nombre patron a buscar
     * @return lista de entidades
     */
    public List<Fabricante> findByNombreLike(String nombre);
    
}
