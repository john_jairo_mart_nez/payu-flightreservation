
package com.payu.service;

import com.payu.model.Ciudad;
import java.util.List;

/**
 * Servicio que maneja las operaciones sobre la entidad ciudad
 * @author johnmartinez
 */
public interface CiudadService {
    
    /**
     * Persiste la entidad 
     * @param ciudad  a persistir
     * @return entidad persistida
     */
    Ciudad save(Ciudad ciudad);
    
    /**
     * Encuentra todas las entidades
     * @return lista de entidades
     */
    List<Ciudad> findAllCiudades();
    
    /**
     * Encuentra una entidad
     * @param id
     * @return entidad
     */
    Ciudad findById(Integer id);
    
    /**
     * Elimina una entidad
     * @param ciudad  a eliminar
     */
    void delete(Ciudad ciudad);

    /**
     * Encuentra una entidad por nombre
     * @param nombre patron a buscar
     * @return lista de entidades
     */
    List<Ciudad> findByNombreLike(String nombre);
    
}
