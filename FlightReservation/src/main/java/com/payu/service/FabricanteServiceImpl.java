/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.payu.service;

import com.payu.model.Fabricante;
import com.payu.repository.FabricanteRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 *
 * @author johnmartinez
 */
@Service
public class FabricanteServiceImpl implements FabricanteService{

    @Autowired
    private FabricanteRepository fabricanteRepository;
    
    public List<Fabricante> findAll() {
        Sort orden = new Sort(Sort.Direction.ASC, "nombre");
        return (List<Fabricante>) fabricanteRepository.findAll(orden);
    }

    public Fabricante findById(Integer id) {
        return fabricanteRepository.findOne(id);
    }

    public Fabricante save(Fabricante fabricante) {
        return fabricanteRepository.save(fabricante);
    }

    public void delete(Fabricante fabricante) {
        fabricanteRepository.delete(fabricante);
    }

    public List<Fabricante> findByNombreLike(String nombre) {
        return fabricanteRepository.findByNombreLike("%" + nombre + "%");
    }
    
}
