package com.payu.service;

import com.payu.model.Aeropuerto;
import java.util.List;

/**
 * Servicio que maneja las operaciones sobre la entidad aeropuerto
 * @author johnmartinez
 */
public interface AeropuertoService {
    
    /**
     * Persiste la entidad 
     * @param aeropuerto a persistir
     * @return entidad persistida
     */
    Aeropuerto save(Aeropuerto aeropuerto);
    
    /**
     * Encuentra todas las entidades
     * @return lista de entidades
     */
    List<Aeropuerto> findAll();
    
    /**
     * Encuentra una entidad
     * @param id
     * @return entidad
     */
    Aeropuerto findById(Integer id);
    
    /**
     * Elimina una entidad
     * @param aeropuerto a eliminar
     */
    void delete(Aeropuerto aeropuerto);

    /**
     * Encuentra una entidad por nombre
     * @param nombre patron a buscar
     * @return lista de entidades
     */
    List<Aeropuerto> findByNombreLike(String nombre);
    
}
