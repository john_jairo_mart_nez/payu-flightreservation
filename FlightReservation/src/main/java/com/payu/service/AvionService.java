
package com.payu.service;

import com.payu.model.Avion;
import com.payu.model.Informe;
import java.util.Date;
import java.util.List;

/**
 * Servicio que maneja las operaciones sobre la entidad avion
 * @author johnmartinez
 */
public interface AvionService {
    
    /**
     * Persiste la entidad 
     * @param avion  a persistir
     * @return entidad persistida
     */
    Avion save(Avion avion);
    
    /**
     * Encuentra todas las entidades
     * @return lista de entidades
     */
    List<Avion> findAll();
    
    /**
     * Encuentra una entidad
     * @param id
     * @return entidad
     */
    Avion findById(Integer id);
    
    /**
     * Elimina una entidad
     * @param avion a eliminar
     */
    void delete(Avion avion);

    /**
     * Encuentra una entidad por nombre
     * @param nombre patron a buscar
     * @return lista de entidades
     */
    List<Avion> findByNombreLike(String nombre);

    /**
     * Genera el reporte de cuantos vuelos y pasajeros por ruta se hacen en un avion
     * @param fechaInicial fecha inicial de busqueda
     * @param fechaFinal fecha final de busqueda
     * @param avionId avion a buscar
     * @return lista de registros del informe
     */
    public List<Informe> generateReport(Date fechaInicial, Date fechaFinal, Integer avionId);
    
}
