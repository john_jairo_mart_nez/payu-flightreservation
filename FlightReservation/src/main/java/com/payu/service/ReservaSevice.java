
package com.payu.service;

import com.payu.model.Reserva;
import java.util.List;

/**
 * Servicio que maneja las operaciones sobre la entidad reserva
 * @author johnmartinez
 */
public interface ReservaSevice {
    
    /**
     * Encuentra todas las entidades
     * @return lista de entidades
     */
    List<Reserva> findAll();
    
    /**
     * Encuentra una entidad
     * @param id
     * @return entidad
     */
    Reserva findById(Integer id);
    
    /**
     * Persiste la entidad 
     * @param reserva a persistir
     * @return entidad persistida
     */
    Reserva save(Reserva reserva);
    
    /**
     * Elimina una entidad
     * @param reserva a eliminar
     */
    void delete(Reserva reserva);
    
}
