
package com.payu.service;

import com.payu.model.Pasajero;
import java.util.List;

/**
 * Servicio que maneja las operaciones sobre la entidad pasajero
 * @author johnmartinez
 */
public interface PasajeroService {
    
    /**
     * Encuentra todas las entidades
     * @return lista de entidades
     */
    List<Pasajero> findAll();
    
    /**
     * Encuentra una entidad
     * @param id
     * @return entidad
     */
    Pasajero findById(Integer id);
    
    /**
     * Persiste la entidad 
     * @param pasajero a persistir
     * @return entidad persistida
     */
    Pasajero save(Pasajero pasajero);
    
    /**
     * Elimina una entidad
     * @param pasajero a eliminar
     */
    void delete(Pasajero pasajero);
    
    /**
     * Encuentra una entidad por nombre
     * @param nombre patron a buscar
     * @return lista de entidades
     */
    List<Pasajero> findByNombreLike(String nombre);
    
}
