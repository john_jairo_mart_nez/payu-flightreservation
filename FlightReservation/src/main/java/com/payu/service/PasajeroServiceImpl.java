/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.payu.service;

import com.payu.model.Pasajero;
import com.payu.repository.PasajeroRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 *
 * @author johnmartinez
 */
@Service
public class PasajeroServiceImpl implements PasajeroService {
    
    @Autowired
    private PasajeroRepository pasajeroRepository;

    public List<Pasajero> findAll() {
        Sort orden = new Sort(Sort.Direction.ASC, "nombre");
        return (List<Pasajero>) pasajeroRepository.findAll(orden);
    }

    public Pasajero findById(Integer id) {
        return pasajeroRepository.findOne(id);
    }

    public Pasajero save(Pasajero pasajero) {
        pasajero = pasajeroRepository.save(pasajero);
        return pasajero;
    }

    public void delete(Pasajero pasajero) {
        pasajeroRepository.delete(pasajero.getId());
    }

    public List<Pasajero> findByNombreLike(String nombre) {
        List<Pasajero> pasajeros = pasajeroRepository.findByNombreLike("%" + nombre + "%");
        return pasajeros;
    }
    
}
