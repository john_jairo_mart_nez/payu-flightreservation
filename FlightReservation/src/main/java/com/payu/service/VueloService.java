/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.payu.service;

import com.payu.model.BuscarVueloViewModel;
import com.payu.model.Vuelo;
import java.util.List;

/**
 * Servicio que maneja las operaciones sobre la entidad vuelo
 * @author johnmartinez
 */
public interface VueloService {
    
    /**
     * Encuentra todas las entidades
     * @return lista de entidades
     */
    List<Vuelo> findAll();
    
    /**
     * Encuentra una entidad
     * @param id
     * @return entidad
     */
    Vuelo findById(Integer id);
    
    /**
     * Persiste la entidad 
     * @param vuelo a persistir
     * @return entidad persistida
     */
    Vuelo save(Vuelo vuelo);
    
    /**
     * Elimina una entidad
     * @param vuelo a eliminar
     */
    void delete(Vuelo vuelo);
    
    /**
     * Encuentra un vuelo por los parámetros de busqueda de un vuelo
     * @param buscarVuelo parametros de busqueda de un vuelo
     * @return lista de vuelos
     */
    List<Vuelo> findBy(BuscarVueloViewModel buscarVuelo);
    
}
