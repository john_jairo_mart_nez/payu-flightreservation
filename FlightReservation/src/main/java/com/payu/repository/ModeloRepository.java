
package com.payu.repository;

import com.payu.model.Modelo;
import java.util.List;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Realiza las operaciones sobre la base de datos para la entidad Modelo
 * @author johnmartinez
 */
@Repository("modeloRepository")
public interface ModeloRepository extends PagingAndSortingRepository<Modelo, Integer>{

    /**
     * Busca los modelos de avion mediante la operación like y un patron sobre el campo nombre
     * @param nombre patron de busqueda
     * @return lista de modelos
     */
    List<Modelo> findByNombreLike(String nombre);
    
}
