package com.payu.repository;

import com.payu.model.Ruta;
import com.payu.model.RutaViewModel;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Realiza las operaciones sobre la base de datos para la entidad Ruta
 * @author johnmartinez
 */
@Repository("rutaRepository")
public interface RutaRepository extends PagingAndSortingRepository<Ruta, Integer>{

    /**
     * Consulta de rutas por nombres de origen y destino
     */
    public final static String FIND_BY_NOMBRE_QUERY = 
            "SELECT new com.payu.model.RutaViewModel("
                + "r.id, CONCAT(o.nombre, ' - ', d.nombre)) " + 
            "FROM Ruta r, Aeropuerto o, Aeropuerto d " +
            "WHERE r.origenId.id = o.id and r.destinoId.id = d.id "
                + "and o.nombre like :origen and d.nombre like :destino ";
    
    /**
     * Consulta de rutas por nombres de origen y destino
     * @param origen 
     * @param destino
     * @return rutas
     */
    @Query(FIND_BY_NOMBRE_QUERY)
    public List<RutaViewModel> findByNombreLike(@Param("origen") String origen, @Param("destino") String destino);
    
}
