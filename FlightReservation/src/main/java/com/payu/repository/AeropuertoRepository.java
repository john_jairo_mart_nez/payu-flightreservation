
package com.payu.repository;

import com.payu.model.Aeropuerto;
import java.util.List;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Encargado de realizar las operaciones con la base de datos sobre la entidad aeropuerto
 * @author johnmartinez
 */
@Repository("aeropuertoRepository")
public interface AeropuertoRepository extends PagingAndSortingRepository<Aeropuerto, Integer>{
    
    /**
     * Busca con operador like por nombre
     * @param nombre a buscar
     * @return lista aeropuertos
     */
    List<Aeropuerto> findByNombreLike(String nombre);
    
}
