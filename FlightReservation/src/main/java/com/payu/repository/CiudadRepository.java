
package com.payu.repository;

import com.payu.model.Ciudad;
import java.util.List;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Realiza las operaciones sobre la base de datos para la entidad Ciudad
 * @author johnmartinez
 */
@Repository("ciudadRepository")
public interface CiudadRepository extends PagingAndSortingRepository<Ciudad, Integer> {
    
    /**
     * Busca las ciudades mediante la operación like y un patron sobre el campo nombre
     * @param nombre patron de busqueda
     * @return lista de ciudades
     */
    List<Ciudad> findByNombreLike(String nombre);
    
}
