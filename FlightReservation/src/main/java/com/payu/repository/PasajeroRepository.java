
package com.payu.repository;

import com.payu.model.Pasajero;
import java.util.List;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Realiza las operaciones sobre la base de datos para la entidad Modelo
 * @author johnmartinez
 */
@Repository("pasajeroRepository")
public interface PasajeroRepository extends PagingAndSortingRepository<Pasajero, Integer> {

    /**
     * Busca los pasajeros mediante la operación like y un patron sobre el campo nombre
     * @param nombre patron de busqueda
     * @return lista de pasajeros
     */
    List<Pasajero> findByNombreLike(String nombre);
    
}
