/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.payu.repository;

import com.payu.model.Fabricante;
import java.util.List;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
  * Realiza las operaciones sobre la base de datos para la entidad Fabricante
 * @author johnmartinez
 */
@Repository("fabricanteRepository")
public interface FabricanteRepository extends PagingAndSortingRepository<Fabricante, Integer>{

    /**
     * Busca los fabricantes mediante la operación like y un patron sobre el campo nombre
     * @param nombre patron de busqueda
     * @return lista de fabricantes
     */
    List<Fabricante> findByNombreLike(String nombre);
    
}
