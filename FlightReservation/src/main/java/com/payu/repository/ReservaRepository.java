/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.payu.repository;

import com.payu.model.Reserva;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Realiza las operaciones sobre la base de datos para la entidad Reserva
 * @author johnmartinez
 */
@Repository("reservaRepository")
public interface ReservaRepository extends PagingAndSortingRepository<Reserva, Integer>{
    
    /**
     * Consulta de reservas por persona y vuelo
     */
    public final static String QUERY_FIND_BY_PERSONA_VUELO = 
            "SELECT r " + 
            "FROM Reserva r " + 
            "WHERE r.pasajeroId.id = :personaId and r.vueloId.id = :vueloId ";
    
    /**
     * Consulta de reservas por persona y vuelo
     * @param personaId id de la persona
     * @param vueloId id del vuelo
     * @return lista de reservas
     */
    @Query(QUERY_FIND_BY_PERSONA_VUELO)
    List<Reserva> FindBy(@Param("personaId") Integer personaId, 
            @Param("vueloId") Integer vueloId);
    
}
