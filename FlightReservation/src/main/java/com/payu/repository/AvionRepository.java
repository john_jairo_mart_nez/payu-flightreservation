package com.payu.repository;

import com.payu.model.Avion;
import com.payu.model.Informe;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Encargado de realizar las operaciones sobre la base de datos de la entidad avion
 * @author johnmartinez
 */
@Repository("avionRepository")
public interface AvionRepository extends PagingAndSortingRepository<Avion, Integer>{
    
    /**
     * Busca mediante una operación like los aviones con nombre similar nombre
     * @param nombre parametros busqueda
     * @return lista de aviones
     */
    List<Avion> findByNombreLike(String nombre);
    
    /**
     * Consulta para generar el informe
     */
    public static final String QUERY_INFORME = 
            "SELECT new com.payu.model.Informe(o.nombre, d.nombre, COUNT(DISTINCT v.id), COUNT(p.id)) " + 
            "FROM Vuelo v JOIN v.rutaId r JOIN r.origenId o JOIN r.destinoId d " +
            "JOIN v.reservaList re JOIN re.pasajeroId p JOIN v.avionId av " + 
            "WHERE v.fechaInicio > :fechaInicio and v.fechaInicio < :fechaFinal and av.id = :avionId " +
            "GROUP BY o.id, o.nombre, d.id, d.nombre ";
    
    /**
     * Genera el informe de rutas recorridas por un avion
     * @param fechaInicio fecha de inicio de busqueda
     * @param fechaFinal fecha final de busqueda
     * @param avionId avion a buscar
     * @return lista de registros del informe de rutas recorridas
     */
    @Query(QUERY_INFORME)
    List<Informe> findReport(@Param("fechaInicio") Date fechaInicio, 
            @Param("fechaFinal") Date fechaFinal, @Param("avionId") Integer avionId);
    
}
