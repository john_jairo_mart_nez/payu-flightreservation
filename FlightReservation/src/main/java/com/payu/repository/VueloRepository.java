
package com.payu.repository;

import com.payu.model.Vuelo;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Realiza las operaciones sobre la base de datos para la entidad Vuelo
 * @author johnmartinez
 */
@Repository("vueloRepository")
public interface VueloRepository extends PagingAndSortingRepository<Vuelo, Integer>{
    
    /**
     * Consulta los vuelos de un avion en un rango de fechas
     */
    public static final String QUERY_VUELOS_DE_AVION = 
            "SELECT v " + 
            "FROM Vuelo v " + 
            "WHERE v.avionId.id = :avionId and (" + 
            "(v.fechaInicio < :fa and v.fechaTerminacion > :fb) or " +
            "(v.fechaInicio > :fa and v.fechaInicio < :fb) or " +
            "(v.fechaTerminacion > :fa and v.fechaTerminacion < :fb) or " +
            "(v.fechaInicio > :fa and v.fechaTerminacion < :fb)" +
            ")";
    
    /**
     * Consulta los vuelos de un avion en un rango de fechas
     * @param avionId identificador del avion
     * @param fa fecha inicial
     * @param fb fecha final
     * @return lista de vuelos encontrados
     */
    @Query(QUERY_VUELOS_DE_AVION)
    List<Vuelo> findVuelosConflicto(@Param("avionId") Integer avionId, 
            @Param("fa") Date fa, @Param("fb") Date fb);
 
    /**
     * Consulta los vuelos para un origen y destino en un rango de fechas
     */
    public static final String QUERY_VUELOS_PARAMETROS = 
            "SELECT v " +
            "FROM Vuelo v JOIN v.rutaId r JOIN r.origenId o JOIN o.ciudadId co JOIN r.destinoId d JOIN d.ciudadId cd  " +
            "WHERE " + 
                "co.id = :origen and cd.id = :destino and " + 
                "v.fechaInicio > :f1 and v.fechaInicio < :f2 and co.id = :origen and cd.id = :destino";
    
    /**
     * Consulta los vuelos para un origen y destino en un rango de fechas
     * @param f1 fecha inicial
     * @param f2 fecha final
     * @param origen ciudad de origen id
     * @param destino ciudad de destino id
     * @return lista de vuelos
     */
    @Query(QUERY_VUELOS_PARAMETROS)
    List<Vuelo> findByFechaCiudad(@Param("f1") Date f1, @Param("f2") Date f2, 
            @Param("origen") Integer origen, @Param("destino") Integer destino);
   
    /**
     * Consulta la capacidad actual de un vuelo
     */
    public static final String QUERY_CAPACIDAD_ACTUAL = 
            "SELECT a.capacidad - count(r.id) " + 
            "FROM Vuelo v JOIN v.avionId a LEFT JOIN v.reservaList r " +
            "WHERE v.id = :vueloId " +
            "GROUP BY a.capacidad ";
    
    /**
     * Consulta la capacidad actual de un vuelo
     * @param vueloId id del vuelo
     * @return capacidad del vuelo
     */
    @Query(QUERY_CAPACIDAD_ACTUAL)
    Long findCapacidadActual(@Param("vueloId") Integer vueloId);
    
}
