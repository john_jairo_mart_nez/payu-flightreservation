
package com.payu.controller;

import com.payu.model.Ruta;
import com.payu.model.RutaViewModel;
import com.payu.service.RutaService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Controlador para realizar operaciones CRUD de la entidad Ruta
 * @author John Jairo Martínez
 */
@Controller
@RequestMapping(value = "/ruta")
public class RutaController {
    
    /**
     * Servicio para manejar la logica de negocio de la entidad ruta
     */
    @Autowired
    private RutaService rutaService;
    
    /**
     * listado de rutas de vuelo
     * @param model inyectado
     * @return listado de rutas de vuelo
     */
    @RequestMapping(value = "/listar")
    public String listar(Model model) {
        List<Ruta> rutas = rutaService.findAll();
        model.addAttribute("rutas", rutas);
        return "listarRuta";
    }
    
    /**
     * retorna vista para crear una ruta de vuelo
     * @param ruta inyectad
     * @return vista para crear una ruta de vuelo
     */
    @RequestMapping(value = "/crear", method = RequestMethod.GET)
    public String crear(@ModelAttribute("ruta") Ruta ruta) {
        return "crearRuta";
    }
    
    /**
     * Crear una ruta de vuelo
     * @param ruta a crear
     * @param result de validacion
     * @return listado de rutas de vuelo
     */
    @RequestMapping(value="/crear", method = RequestMethod.POST)
    public String crear(@Valid @ModelAttribute("ruta") Ruta ruta, BindingResult result) {
        if(result.hasErrors()){
            return "crearRuta";
        }
        rutaService.save(ruta);
        return "redirect:listar.html";
    }
    
    /**
     * retorna vista para editar ruta de vuelo
     * @param id de ruta de vuelo
     * @param model inyectado
     * @return vista para editar ruta de vuelo
     */
    @RequestMapping(value = "/editar", method = RequestMethod.GET)
    public String editar(@RequestParam("id") Integer id, Model model) {
        Ruta ruta = rutaService.findById(id);
        model.addAttribute("ruta", ruta);
        return "editarRuta";
    }
    
    /**
     * edita ruta de vuelo
     * @param ruta editada
     * @param result de validacion
     * @return listado de rutas
     */
    @RequestMapping(value = "/editar", method = RequestMethod.POST)
    public String editar(@Valid @ModelAttribute("ruta") Ruta ruta, BindingResult result){
        if(result.hasErrors() || ruta.getId() == null){
            return "editarRuta";
        }
        rutaService.save(ruta);
        return "redirect:listar.html";
    }
    
    /**
     * retorna vista para eliminar ruta
     * @param id de la ruta
     * @param model inyectado
     * @return vista para eliminar ruta
     */
    @RequestMapping(value = "/eliminar", method = RequestMethod.GET)
    public String eliminar(@RequestParam("id") Integer id, Model model){
        Ruta ruta = rutaService.findById(id);
        model.addAttribute("ruta", ruta);
        return "eliminarRuta";
    }
    
    /**
     * elimina ruta
     * @param ruta a eliminar
     * @param result de validacion
     * @return listado de rutas
     */
    @RequestMapping(value = "/eliminar", method = RequestMethod.POST)
    public String eliminar(@Valid @ModelAttribute("ruta") Ruta ruta, BindingResult result){
        if(result.hasErrors() || ruta.getId() == null){
            return "eliminarRuta";
        }
        rutaService.delete(ruta);
        return "redirect:listar.html";
    }
    
    /**
     * buscar rutas por nombre de la ruta
     * @param nombre nombre de la ruta
     * @return listado de rutas
     */
    @RequestMapping(value = "/buscarPorNombre", method = RequestMethod.POST)
    public @ResponseBody List<RutaViewModel> buscarPorNombre(@RequestParam("nombre") String nombre){
        return rutaService.findByNombreLike(nombre);
    }
    
}
