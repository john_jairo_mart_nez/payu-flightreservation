
package com.payu.controller;

import com.payu.model.Vuelo;
import com.payu.service.VueloService;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Controlador para realizar operaciones CRUD sobre la entidad Vuelo
 * @author John Jairo Martínez
 */
@Controller
@RequestMapping("/vuelo")
public class VueloController {
    
    /**
     * Logica de negocio de la entidad vuelo
     */
    @Autowired
    private VueloService vueloService;
    
    /**
     * Define el formato para mapear datos tipo Date
     * @param binder 
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
    }
    
    /**
     * retorna vista con listado de vuelos
     * @param model inyectado
     * @param msgError indica si existió algun error
     * @return vista con listado de vuelos
     */
    @RequestMapping(value = "/listar")
    public String listar(Model model, @ModelAttribute("msgError") String msgError) {
        List<Vuelo> vuelos = vueloService.findAll();
        model.addAttribute("vuelos", vuelos);
        model.addAttribute("msgError", msgError);
        return "listarVuelo";
    }
    
    /**
     * retorna vista para crear vuelo
     * @param vuelo 
     * @return vista para crear vuelo
     */
    @RequestMapping(value = "/crear", method = RequestMethod.GET)
    public String crear(@ModelAttribute("vuelo") Vuelo vuelo) {
        vuelo.setFechaInicio(new Date(2015, 07, 15, 11, 10, 00));
        return "crearVuelo";
    }
    
    /**
     * crea un vuelo
     * @param vuelo a crear
     * @param result de validacion
     * @param redirectAttrs atributos para redirect
     * @return lista de vuelos
     */
    @RequestMapping(value="/crear", method = RequestMethod.POST)
    public String crear(@Valid @ModelAttribute("vuelo") Vuelo vuelo, BindingResult result,
            RedirectAttributes redirectAttrs) {
        if(result.hasErrors()){
            return "crearVuelo";
        }
        if(vueloService.save(vuelo) == null){
            redirectAttrs.addFlashAttribute("msgError", "No se puede crear el vuelo seleccionado");
        }
        return "redirect:listar.html";
    }
    
    /**
     * retorna la vista para editar un vuelo
     * @param id del vuelo
     * @param model inyectado
     * @return vista para editar un vuelo
     */
    @RequestMapping(value = "/editar", method = RequestMethod.GET)
    public String editar(@RequestParam("id") Integer id, Model model) {
        Vuelo vuelo = vueloService.findById(id);
        model.addAttribute("vuelo", vuelo);
        return "editarVuelo";
    }
    
    /**
     * edita un vuelo
     * @param vuelo editado
     * @param result de validacion
     * @param redirectAttrs atributos de redirección
     * @return listado de vuelos
     */
    @RequestMapping(value = "/editar", method = RequestMethod.POST)
    public String editar(@Valid @ModelAttribute("vuelo") Vuelo vuelo, BindingResult result,
            RedirectAttributes redirectAttrs){
        if(result.hasErrors() || vuelo.getId() == null){
            return "editarVuelo";
        }
        if(vueloService.save(vuelo) == null){
            redirectAttrs.addFlashAttribute("msgError", "No se puede editar el vuelo seleccionado");
        }
        return "redirect:listar.html";
    }
    
    /**
     * retorna vista para eliminar vuelo
     * @param id del vuelo
     * @param model inyectado
     * @return vista para eliminar vuelo
     */
    @RequestMapping(value = "/eliminar", method = RequestMethod.GET)
    public String eliminar(@RequestParam("id") Integer id, Model model){
        Vuelo vuelo = vueloService.findById(id);
        model.addAttribute("vuelo", vuelo);
        return "eliminarVuelo";
    }
    
    /**
     * Elimina vuelo
     * @param vuelo 
     * @param result
     * @return 
     */
    @RequestMapping(value = "/eliminar", method = RequestMethod.POST)
    public String eliminar(@Valid @ModelAttribute("vuelo") Vuelo vuelo, BindingResult result){
        if(result.hasErrors() || vuelo.getId() == null){
            return "eliminarVuelo";
        }
        vueloService.delete(vuelo);
        return "redirect:listar.html";
    }
    
    
}
