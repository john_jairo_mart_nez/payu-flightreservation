package com.payu.controller;

import com.payu.model.Fabricante;
import com.payu.service.FabricanteService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Controlador para operaciones CRUD de la entidad Fabricante
 * @author John Jairo Martínez
 */
@Controller
@RequestMapping("/fabricante")
public class FabricanteController {
    
    /**
     * Servicio para manejar la lógica de negocio del fabricante
     */
    @Autowired
    private FabricanteService fabricanteService;
    
    /**
     * Retorna vista listado de fabricantes
     * @param model modelo inyectado por spring
     * @return vista listado de fabricantes
     */
    @RequestMapping(value = "/listar", method = RequestMethod.GET)
    public String listar(Model model){
        List<Fabricante> fabricantes = fabricanteService.findAll();
        model.addAttribute("fabricantes", fabricantes);
        return "listarFabricante";
    }
    
    /**
     * Retorna vista para crear un fabricante
     * @param fabricante modelo inyectado por spring
     * @return vista para crear un fabricante
     */
    @RequestMapping(value = "/crear", method = RequestMethod.GET)
    public String crear(@ModelAttribute("fabricante") Fabricante fabricante){
        return "crearFabricante";
    }
    
    /**
     * Crea un fabricante
     * @param fabricante fabricante a crear
     * @param result resultado de validación
     * @return vista listado de fabricantes
     */
    @RequestMapping(value = "/crear", method = RequestMethod.POST)
    public String crear(@Valid @ModelAttribute("fabricante") Fabricante fabricante, BindingResult result){
        if(result.hasErrors()){
            return "crearFabricante";
        }
        fabricanteService.save(fabricante);
        return "redirect:listar.html";
    }
    
    /**
     * Retorna vista para editar un fabricante
     * @param id identificador del fabricante
     * @param model modelo inyectado por mvc
     * @return vista para editar un fabricante
     */
    @RequestMapping(value="/editar", method = RequestMethod.GET)
    public String editar(@RequestParam("id") Integer id, Model model){
        Fabricante fabricante = fabricanteService.findById(id);
        model.addAttribute("fabricante", fabricante);
        return "editarFabricante";
    }
    
    /**
     * Edita un fabricante
     * @param fabricante editado
     * @param result resultado de validacion
     * @return vista listado de fabricatnes
     */
    @RequestMapping(value = "/editar", method = RequestMethod.POST)
    public String editar(@Valid @ModelAttribute("fabricante") Fabricante fabricante, BindingResult result){
        if(result.hasErrors() || fabricante.getId() == null){
            return "editarFabricante";
        }
        fabricanteService.save(fabricante);
        return "redirect:listar.html";
    }
    
    /**
     * Retorna vista para eliminar un fabricante
     * @param id identificador del fabricante
     * @param model modelo inyectado
     * @return vista para eliminar un fabricante
     */
    @RequestMapping(value = "/eliminar", method = RequestMethod.GET)
    public String eliminar(@RequestParam("id") Integer id, Model model){
        Fabricante fabricante = fabricanteService.findById(id);
        model.addAttribute("fabricante", fabricante);
        return "eliminarFabricante";
    }
    
    /**
     * Elimina un fabricante
     * @param fabricante a eliminar
     * @param result resultado de validacion
     * @return vista listado de fabricantes
     */
    @RequestMapping(value = "/eliminar", method = RequestMethod.POST)
    public String eliminar(@Valid @ModelAttribute("fabricante") Fabricante fabricante, BindingResult result){
        if(result.hasErrors() || fabricante.getId() == null){
            return "eliminarFabricante";
        }
        fabricanteService.delete(fabricante);
        return "redirect:listar.html";
    }
    
    /**
     * Busca fabricantes en base a su nombre
     * @param nombre patrón de busqueda
     * @return lista de fabricantes
     */
    @RequestMapping(value = "/buscarPorNombre", method = RequestMethod.POST)
    public @ResponseBody List<Fabricante> buscarPorNombre(@RequestParam("nombre") String nombre){
        List<Fabricante> fabricantes = fabricanteService.findByNombreLike(nombre);
        return fabricantes;
    }
    
}
