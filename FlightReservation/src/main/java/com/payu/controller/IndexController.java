package com.payu.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Controlador para operaciones de bienvenida
 * @author johnmartinez
 */
@Controller
public class IndexController {
    
    /**
     * Muestra la pagina de bienvenida
     * @return pagina de bienvenida
     */
    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String index(){
        return "index";
    }
    
}
