package com.payu.controller;

import com.payu.model.BuscarVueloViewModel;
import com.payu.model.Reserva;
import com.payu.model.Vuelo;
import com.payu.service.ReservaSevice;
import com.payu.service.VueloService;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Controlador que maneja el CRUD de las reservas de vuelos
 * @author John Jairo Martínez
 */
@Controller
@RequestMapping("/reserva")
public class ReservaController {
    
    /**
     * Maneja la logica de negocio de una reserva
     */
    @Autowired
    private ReservaSevice reservaService;
    
    /**
     * Maneja la logica de negocio de un vuelo
     */
    @Autowired
    private VueloService vueloService;
    
    /**
     * Define la manera en que el controlador va a convertir las fechas
     * @param binder 
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
    }
    
    /**
     * Retorna vista para buscar un vuelo
     * @param model inyectado
     * @return vista para buscar un vuelo
     */
    @RequestMapping(value = "/buscarVuelo", method = RequestMethod.GET)
    public String buscarVuelo(Model model){
        model.addAttribute("buscarVuelo", new BuscarVueloViewModel());
        return "buscarVueloReserva";
    }
    
    /**
     * Retorna listado de vuelos para reservar
     * @param buscarVuelo parametros de busqueda de vuelos a reservar
     * @param result de validacion
     * @param model inyectado
     * @return listado de vuelos para reservar
     */
    @RequestMapping(value = "/listarVuelo", method = RequestMethod.POST)
    public String listarVuelo(@Valid @ModelAttribute("buscarVuelo") BuscarVueloViewModel buscarVuelo, 
            BindingResult result, Model model){
        if(result.hasErrors()){
            return "redirect:buscarVuelo.html";
        }
        List<Vuelo> vuelos = vueloService.findBy(buscarVuelo);
        model.addAttribute("vuelos", vuelos);
        return "listarVueloReserva";
    }
    
    /**
     * Muestra una lista completa de las reservas existentes
     * @param model inyectado
     * @param msgError mensaje de error recibido del redirect
     * @return listado de reservas
     */
    @RequestMapping(value = "/listar", method = RequestMethod.GET)
    public String listar(Model model, @ModelAttribute("msgError") String msgError) {
        List<Reserva> reservas = reservaService.findAll();
        model.addAttribute("reservas", reservas);
        model.addAttribute("msgError", msgError);
        return "listarReserva";
    }
    
    /**
     * retorna vista para crear una reserva
     * @param vueloId vuelo de la reserva
     * @param model inyectado
     * @return vista para crear una reserva
     */
    @RequestMapping(value = "/crear", method = RequestMethod.GET)
    public String crear(@RequestParam("vueloId") Integer vueloId, Model model) {
        Vuelo vuelo = vueloService.findById(vueloId);
        model.addAttribute("vuelo", vuelo);
        model.addAttribute("reserva", new Reserva());
        return "crearReserva";
    }
    
    /**
     * Crea una reserva
     * @param reserva a crear
     * @param result de validacion
     * @param redirectAttrs atributos redirección
     * @return listado de vuelos
     */
    @RequestMapping(value="/crear", method = RequestMethod.POST)
    public String crear(@Valid @ModelAttribute("reserva") Reserva reserva, BindingResult result,
            RedirectAttributes redirectAttrs) {
        if(result.hasErrors()){
            return "crearReserva";
        }
        if(reservaService.save(reserva) == null){
            redirectAttrs.addFlashAttribute("msgError", "No se pudo crear la reserva, rompe las reglas de negocio");
        }
        return "redirect:listar.html";
    }
    
    /**
     * Retorna vista para editar reserva
     * @param id de la reserva
     * @param model inyectado
     * @return vista para editar reserva
     */
    @RequestMapping(value = "/editar", method = RequestMethod.GET)
    public String editar(@RequestParam("id") Integer id, Model model) {
        Reserva reserva = reservaService.findById(id);
        model.addAttribute("reserva", reserva);
        return "editarReserva";
    }
    
    /**
     * Edita la reserva
     * @param reserva editada
     * @param result de validacion
     * @param redirectAttrs atributos redireccion
     * @return listado de reservas
     */
    @RequestMapping(value = "/editar", method = RequestMethod.POST)
    public String editar(@Valid @ModelAttribute("reserva") Reserva reserva, BindingResult result, 
            RedirectAttributes redirectAttrs){
        if(result.hasErrors() || reserva.getId() == null){
            return "editarReserva";
        }
        if(reservaService.save(reserva) == null){
            redirectAttrs.addFlashAttribute("msgError", "No se pudo editar la reserva, rompe las reglas de negocio");
        }
        return "redirect:listar.html";
    }
    
    /**
     * retorna vista para eliminar una reserva
     * @param id
     * @param model
     * @return vista para eliminar una reserva
     */
    @RequestMapping(value = "/eliminar", method = RequestMethod.GET)
    public String eliminar(@RequestParam("id") Integer id, Model model){
        Reserva reserva = reservaService.findById(id);
        model.addAttribute("reserva", reserva);
        return "eliminarReserva";
    }
    
    /**
     * elimina una reserva
     * @param reserva a eliminar
     * @param result de validacion
     * @return listado de reservas
     */
    @RequestMapping(value = "/eliminar", method = RequestMethod.POST)
    public String eliminar(@Valid @ModelAttribute("reserva") Reserva reserva, BindingResult result){
        if(result.hasErrors() || reserva.getId() == null){
            return "eliminarReserva";
        }
        reservaService.delete(reserva);
        return "redirect:listar.html";
    }
    
}
