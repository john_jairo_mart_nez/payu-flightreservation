
package com.payu.controller;

import com.payu.model.Informe;
import com.payu.model.InformeViewModel;
import com.payu.service.AvionService;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Controlador para generación de informes
 * @author John Jairo Martínez
 */
@Controller
@RequestMapping(value = "/informe")
public class InformeController {
    
    /**
     * Servicio que realiza operaciones sobre la entidad avion, adicionalmente genera reporte
     */
    @Autowired
    private AvionService avionService;
    
    /**
     * Define parámetros de binding de la vista al controlador para fechas. Usando formato yyyy/MM/dd
     * @param binder 
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
    }
    
    /**
     * Muestra el formulario con los parametros para generar el informe
     * @param model inyectado por spring
     * @return formulario con los parametros para generar el informe
     */
    @RequestMapping(value = "/buscarInforme", method = RequestMethod.GET)
    public String buscarInfome(Model model){
        model.addAttribute("informe", new InformeViewModel());
        return "buscarInforme";
    }
    
    /**
     * Vista que incluye el informe generado
     * @param informe parametros para generar el informe
     * @param result resultado de validacion de los parametros
     * @param model modelo inyectado
     * @return informe generado
     */
    @RequestMapping(value = "/buscarInforme", method = RequestMethod.POST)
    public String generarInforme(@Valid @ModelAttribute("informe") InformeViewModel informe, 
            BindingResult result, Model model) {
        if(result.hasErrors()){
            return "buscarInforme";
        }
        List<Informe> resultados = avionService.generateReport(informe.getFechaInicial(), informe.getFechaFinal(), informe.getAvionId());
        model.addAttribute("resultados", resultados);
        return "generarInforme";
    }
        
}
