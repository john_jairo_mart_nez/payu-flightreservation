package com.payu.controller;

import com.payu.model.Ciudad;
import com.payu.service.CiudadService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Controlador para operaciones CRUD de la entidad Ciudad
 * @author johnmartinez
 */
@Controller
@RequestMapping(value = "/ciudad")
public class CiudadController {
    
    /**
     * Servicio que maneja la lógica de negocio de la entidad Ciudad
     */
    @Autowired
    private CiudadService ciudadService;
    
    /**
     * Retorna vista con la lista de ciudades
     * @param model modelo inyectado por spring
     * @return vista con la lista de ciudades
     */
    @RequestMapping(value = "/listar")
    public String listar(Model model) {
        List<Ciudad> ciudades = ciudadService.findAllCiudades();
        model.addAttribute("ciudades", ciudades);
        return "listarCiudad";
    }
    
    /**
     * Retorna vista para crear una ciudad
     * @param ciudad modelo inyectado
     * @return vista para crear una ciudad
     */
    @RequestMapping(value = "/crear", method = RequestMethod.GET)
    public String crear(@ModelAttribute("ciudad") Ciudad ciudad) {
        return "crearCiudad";
    }
    
    /**
     * Crea una ciudad
     * @param ciudad que se va a crear
     * @param result resultado de validación
     * @return listado con la ciudad creada
     */
    @RequestMapping(value="/crear", method = RequestMethod.POST)
    public String crear(@Valid @ModelAttribute("ciudad") Ciudad ciudad, BindingResult result) {
        if(result.hasErrors()){
            return "crearCiudad";
        }
        ciudadService.save(ciudad);
        return "redirect:listar.html";
    }
    
    /**
     * Retorna vista para editar una ciudad
     * @param id identificador de la ciudad 
     * @param model modelo inyectado
     * @return vista para editar una ciudad
     */
    @RequestMapping(value = "/editar", method = RequestMethod.GET)
    public String editar(@RequestParam("id") Integer id, Model model) {
        Ciudad ciudad = ciudadService.findById(id);
        model.addAttribute("ciudad", ciudad);
        return "editarCiudad";
    }
    
    /**
     * Edita una ciudad
     * @param ciudad ciudad con la información editada
     * @param result resultado de validación
     * @return listado de ciudades
     */
    @RequestMapping(value = "/editar", method = RequestMethod.POST)
    public String editar(@Valid @ModelAttribute("ciudad") Ciudad ciudad, BindingResult result){
        if(result.hasErrors() || ciudad.getId() == null){
            return "editarCiudad";
        }
        ciudadService.save(ciudad);
        return "redirect:listar.html";
    }
    
    /**
     * Retorna vista para eliminar una ciudad
     * @param id identificador de la ciudad
     * @param model modelo inyectado por spring
     * @return vista para eliminar una ciudad
     */
    @RequestMapping(value = "/eliminar", method = RequestMethod.GET)
    public String eliminar(@RequestParam("id") Integer id, Model model){
        Ciudad ciudad = ciudadService.findById(id);
        model.addAttribute("ciudad", ciudad);
        return "eliminarCiudad";
    }
    
    /**
     * Elimina una ciudad
     * @param ciudad a eliminar
     * @param result resultado de validación
     * @return vista de lista de ciudades
     */
    @RequestMapping(value = "/eliminar", method = RequestMethod.POST)
    public String eliminar(@Valid @ModelAttribute("ciudad") Ciudad ciudad, BindingResult result){
        if(result.hasErrors() || ciudad.getId() == null){
            return "eliminarCiudad";
        }
        ciudadService.delete(ciudad);
        return "redirect:listar.html";
    }
    
    /**
     * Busca lista de ciudades basado en un nombre
     * @param nombre parametro de busqueda
     * @return lista de ciudades
     */
    @RequestMapping(value = "/buscarPorNombre", method = RequestMethod.POST)
    public @ResponseBody List<Ciudad> buscarPorNombre(@RequestParam("nombre") String nombre){
        List<Ciudad> ciudades = ciudadService.findByNombreLike(nombre);
        return ciudades;
    }
    
}
