package com.payu.controller;

import com.payu.model.Avion;
import com.payu.service.AvionService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Controlador para realizar operaciones CRUD sobre la entidad Avion
 * @author John Jairo Martínez
 */
@Controller
@RequestMapping(value = "/avion")
public class AvionController {
    
    /**
     * Servicio con la lógica de negocio para la entidad avión
     */
    @Autowired
    private AvionService avionService;
    
    /**
     * Retorna la vista listado de aviones
     * @param model
     * @return 
     */
    @RequestMapping(value = "/listar")
    public String listar(Model model) {
        List<Avion> avions = avionService.findAll();
        model.addAttribute("aviones", avions);
        return "listarAvion";
    }
    
    /**
     * Retorna la vista para crear un avión
     * @param avion modelo inyectado por spring
     * @return vista para crear un avión
     */
    @RequestMapping(value = "/crear", method = RequestMethod.GET)
    public String crear(@ModelAttribute("avion") Avion avion) {
        return "crearAvion";
    }
    
    /**
     * Crea un avión 
     * @param avion avion a crear
     * @param result resultado de validación
     * @return vista listado que incluye el avion creado
     */
    @RequestMapping(value="/crear", method = RequestMethod.POST)
    public String crear(@Valid @ModelAttribute("avion") Avion avion, BindingResult result) {
        if(result.hasErrors()){
            return "crearAvion";
        }
        avionService.save(avion);
        return "redirect:listar.html";
    }
    
    /**
     * Retorna vista para editar un avión
     * @param id identificador del avión
     * @param model modelo inyectado por spring
     * @return Vista para editar un avión
     */
    @RequestMapping(value = "/editar", method = RequestMethod.GET)
    public String editar(@RequestParam("id") Integer id, Model model) {
        Avion avion = avionService.findById(id);
        model.addAttribute("avion", avion);
        return "editarAvion";
    }
    
    /**
     * Edita un avión 
     * @param avion avion a crear
     * @param result resultado de validación
     * @return vista de listado con el avión editado
     */
    @RequestMapping(value = "/editar", method = RequestMethod.POST)
    public String editar(@Valid @ModelAttribute("avion") Avion avion, BindingResult result){
        if(result.hasErrors() || avion.getId() == null){
            return "editarAvion";
        }
        avionService.save(avion);
        return "redirect:listar.html";
    }
    
    /**
     * Muestra la vista para eliminar un avion
     * @param id identificador del avion
     * @param model modelo inyectado por spring
     * @return vista para eliminar un avion
     */
    @RequestMapping(value = "/eliminar", method = RequestMethod.GET)
    public String eliminar(@RequestParam("id") Integer id, Model model){
        Avion avion = avionService.findById(id);
        model.addAttribute("avion", avion);
        return "eliminarAvion";
    }
    
    /**
     * Elimina un avion 
     * @param avion avion a eliminar
     * @param result resultado de validacion
     * @return vista listado con el avion eliminado
     */
    @RequestMapping(value = "/eliminar", method = RequestMethod.POST)
    public String eliminar(@Valid @ModelAttribute("avion") Avion avion, BindingResult result){
        if(result.hasErrors() || avion.getId() == null){
            return "eliminarAvion";
        }
        avionService.delete(avion);
        return "redirect:listar.html";
    }
    
    /**
     * Busca un avión con base en el nombre dado como parámetro
     * @param nombre patron de busqueda
     * @return lista de aviones
     */
    @RequestMapping(value = "/buscarPorNombre", method = RequestMethod.POST)
    public @ResponseBody List<Avion> buscarPorNombre(@RequestParam("nombre") String nombre){
        return avionService.findByNombreLike(nombre);
    }
    
}
