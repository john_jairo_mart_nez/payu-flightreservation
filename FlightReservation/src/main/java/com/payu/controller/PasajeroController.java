package com.payu.controller;

import com.payu.model.Pasajero;
import com.payu.service.PasajeroService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Controlador para manejar las operaciones CRUD de la entidad Pasajero
 * @author johnmartinez
 */
@Controller
@RequestMapping(value = "/pasajero")
public class PasajeroController {
    
    /**
     * Servicio que maneja la logica de negocio sobre la entidad pasajero
     */
    @Autowired
    private PasajeroService pasajeroService;
    
    /**
     * Retorna vista con el listado de pasajeros
     * @param model inyectado
     * @return vista con el listado de pasajeros
     */
    @RequestMapping(value = "/listar")
    public String listar(Model model) {
        List<Pasajero> pasajeros = pasajeroService.findAll();
        model.addAttribute("pasajeros", pasajeros);
        return "listarPasajero";
    }
    
    /**
     * Vista para crear un pasajero 
     * @param pasajero inyectado
     * @return vista para crear un pasajero
     */
    @RequestMapping(value = "/crear", method = RequestMethod.GET)
    public String crear(@ModelAttribute("pasajero") Pasajero pasajero) {
        return "crearPasajero";
    }
    
    /**
     * Crea un nuevo pasajero
     * @param pasajero a crear
     * @param result resultado de validacion
     * @return vista con el listado de pasajeros
     */
    @RequestMapping(value="/crear", method = RequestMethod.POST)
    public String crear(@Valid @ModelAttribute("pasajero") Pasajero pasajero, BindingResult result) {
        if(result.hasErrors()){
            return "crearPasajero";
        }
        pasajeroService.save(pasajero);
        return "redirect:listar.html";
    }
    
    /**
     * Retorna vista para editar un pasajero
     * @param id del pasajero a editar
     * @param model inyectado
     * @return vista para editar un pasajero
     */
    @RequestMapping(value = "/editar", method = RequestMethod.GET)
    public String editar(@RequestParam("id") Integer id, Model model) {
        Pasajero pasajero = pasajeroService.findById(id);
        model.addAttribute("pasajero", pasajero);
        return "editarPasajero";
    }
    
    /**
     * Edita un pasajero
     * @param pasajero editado
     * @param result resultado de validación
     * @return vista con listado de pasajeros
     */
    @RequestMapping(value = "/editar", method = RequestMethod.POST)
    public String editar(@Valid @ModelAttribute("pasajero") Pasajero pasajero, BindingResult result){
        if(result.hasErrors() || pasajero.getId() == null){
            return "editarPasajero";
        }
        pasajeroService.save(pasajero);
        return "redirect:listar.html";
    }
    
    /**
     * retorna vista para eliminar un pasajero
     * @param id del pasajero
     * @param model inyectado
     * @return vista para eliminar un pasajero
     */
    @RequestMapping(value = "/eliminar", method = RequestMethod.GET)
    public String eliminar(@RequestParam("id") Integer id, Model model){
        Pasajero pasajero = pasajeroService.findById(id);
        model.addAttribute("pasajero", pasajero);
        return "eliminarPasajero";
    }
    
    /**
     * Elimina un pasajero 
     * @param pasajero a eliminar
     * @param result resultado de validacion
     * @return listado de pasajeros
     */
    @RequestMapping(value = "/eliminar", method = RequestMethod.POST)
    public String eliminar(@Valid @ModelAttribute("pasajero") Pasajero pasajero, BindingResult result){
        if(result.hasErrors() || pasajero.getId() == null){
            return "eliminarPasajero";
        }
        pasajeroService.delete(pasajero);
        return "redirect:listar.html";
    }
    
    /**
     * Busca pasajeros de acuerdo a su nombre
     * @param nombre parametro de busqueda
     * @return listado de pasajeros
     */
    @RequestMapping(value = "/buscarPorNombre", method = RequestMethod.POST)
    public @ResponseBody List<Pasajero> buscarPorNombre(@RequestParam("nombre") String nombre){
        return pasajeroService.findByNombreLike(nombre);
    }
    
}
