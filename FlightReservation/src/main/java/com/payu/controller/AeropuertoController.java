package com.payu.controller;

import com.payu.model.Aeropuerto;
import com.payu.service.AeropuertoService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Controlador encargado de las operaciones tipo CRUD sobre la entidad Aeropuerto.
 * @author John Jairo Martínez
 */
@Controller
@RequestMapping(value = "/aeropuerto")
public class AeropuertoController {
    
    /**
     * Servicio para lógica de negocio de aeropuerto
     */
    @Autowired
    private AeropuertoService aeropuertoService;
    
    /**
     * Retorna vista con listado completo de aeropuertos
     * @param model Modelo inyectado por Spring
     * @return vista lista de aeropuertos
     */
    @RequestMapping(value = "/listar")
    public String listar(Model model) {
        List<Aeropuerto> aeropuertos = aeropuertoService.findAll();
        model.addAttribute("aeropuertos", aeropuertos);
        return "listarAeropuerto";
    }
    
    /**
     * Retorna vista para crear un aeropuerto
     * @param aeropuerto Modelo inyectado por spring
     * @return vista para crear aeropuerto
     */
    @RequestMapping(value = "/crear", method = RequestMethod.GET)
    public String crear(@ModelAttribute("aeropuerto") Aeropuerto aeropuerto) {
        return "crearAeropuerto";
    }
    
    /**
     * Agrega aeropuerto y retorna a la vista de listado
     * @param aeropuerto aeropuerto a agregar
     * @param result resultado de validación del aeropuerto
     * @return vista con listado de aeropuerto
     */
    @RequestMapping(value="/crear", method = RequestMethod.POST)
    public String crear(@Valid @ModelAttribute("aeropuerto") Aeropuerto aeropuerto, BindingResult result) {
        if(result.hasErrors()){
            return "crearAeropuerto";
        }
        aeropuertoService.save(aeropuerto);
        return "redirect:listar.html";
    }
    
    /**
     * Muestra la vista para editar un aeropuerto
     * @param id identificador del aeropuerto
     * @param model modelo inyectado por spring
     * @return vista para editar el aeropuerto
     */
    @RequestMapping(value = "/editar", method = RequestMethod.GET)
    public String editar(@RequestParam("id") Integer id, Model model) {
        Aeropuerto aeropuerto = aeropuertoService.findById(id);
        model.addAttribute("aeropuerto", aeropuerto);
        return "editarAeropuerto";
    }
    
    /**
     * Edita la información almacenada de un aeropuerto
     * @param aeropuerto aeropuerto con la informacion editada
     * @param result resultado de validación
     * @return vista de listado de aeropuertos
     */
    @RequestMapping(value = "/editar", method = RequestMethod.POST)
    public String editar(@Valid @ModelAttribute("aeropuerto") Aeropuerto aeropuerto, BindingResult result){
        if(result.hasErrors() || aeropuerto.getId() == null){
            return "editarAeropuerto";
        }
        aeropuertoService.save(aeropuerto);
        return "redirect:listar.html";
    }
    
    /**
     * Muestra la vista para eliminar un aeropuerto
     * @param id identificador del aeropuerto
     * @param model modelo inyectado por spring
     * @return vista para eliminar aeropuerto
     */
    @RequestMapping(value = "/eliminar", method = RequestMethod.GET)
    public String eliminar(@RequestParam("id") Integer id, Model model){
        Aeropuerto aeropuerto = aeropuertoService.findById(id);
        model.addAttribute("aeropuerto", aeropuerto);
        return "eliminarAeropuerto";
    }
    
    /**
     * Elimina un aeropuerto
     * @param aeropuerto aeropuerto a eliminar
     * @param result resultuado de validación
     * @return vista listado de aeropuertos
     */
    @RequestMapping(value = "/eliminar", method = RequestMethod.POST)
    public String eliminar(@Valid @ModelAttribute("aeropuerto") Aeropuerto aeropuerto, BindingResult result){
        if(result.hasErrors() || aeropuerto.getId() == null){
            return "eliminarAeropuerto";
        }
        aeropuertoService.delete(aeropuerto);
        return "redirect:listar.html";
    }
    
    /**
     * Busca los aeropuertos con nombre similar al proporcionado
     * @param nombre patron de nombre a buscar
     * @return  lista de aeropuertos encontrados
     */
    @RequestMapping(value = "/buscarPorNombre", method = RequestMethod.POST)
    public @ResponseBody List<Aeropuerto> buscarPorNombre(@RequestParam("nombre") String nombre){
        return aeropuertoService.findByNombreLike(nombre);
    }
    
}
