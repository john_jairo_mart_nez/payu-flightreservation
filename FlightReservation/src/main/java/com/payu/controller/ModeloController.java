package com.payu.controller;

import com.payu.model.Modelo;
import com.payu.service.ModeloService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Controlador para operaciones CRUD de modelos de aviones
 * @author John Jairo Martinez
 */
@Controller
@RequestMapping("/modelo")
public class ModeloController {
    
    /**
     * Servicio que maneja la lógica de negocio de los modelos de aviones
     */
    @Autowired
    private ModeloService modeloService;
    
    /**
     * Retorna vista con el listado de modelos de avion
     * @param model inyectado por spring
     * @return vista con el listado de modelos de avion
     */
    @RequestMapping(value = "/listar", method = RequestMethod.GET)
    public String listar(Model model){
        List<Modelo> modelos = modeloService.findAll();
        model.addAttribute("modelos", modelos);
        return "listarModelo";
    }
    
    
    /**
     * Retorna vista para crear un modelo de avion
     * @param modelo de avion inyectado
     * @return vista para crear un modelo de avion
     */
    @RequestMapping(value = "/crear", method = RequestMethod.GET)
    public String crear(@ModelAttribute("modelo") Modelo modelo){
        return "crearModelo";
    }
    
    /**
     * Retorna vista para crear un modelo de avion
     * @param modelo a crear
     * @param result resultado de validación
     * @return listado de modelos de avion
     */
    @RequestMapping(value = "/crear", method = RequestMethod.POST)
    public String crear(@Valid @ModelAttribute("modelo") Modelo modelo, BindingResult result){
        if(result.hasErrors()){
            return "crearModelo";
        }
        modeloService.save(modelo);
        return "redirect:listar.html";
    }
    
    /**
     * Retorna vista para editar un modelo de avion
     * @param id identificador del modleo de avion
     * @param model inyectado
     * @return vista para editar un modelo de avion
     */
    @RequestMapping(value = "/editar", method = RequestMethod.GET)
    public String editar(@RequestParam("id") Integer id, Model model) {
        Modelo modelo = modeloService.findById(id);
        model.addAttribute("modelo", modelo);
        return "editarModelo";
    }
    
    /**
     * Edita un modelo de avion
     * @param modelo editado
     * @param result resultado de validacion
     * @return  listaod de modelos de avion
     */
    @RequestMapping(value = "/editar", method = RequestMethod.POST)
    public String editar(@Valid @ModelAttribute("modelo") Modelo modelo, BindingResult result){
        if(result.hasErrors() || modelo.getId() == null){
            return "editarModelo";
        }
        modeloService.save(modelo);
        return "redirect:listar.html";
    }
    
    /**
     * Retorna vista para eliminar un modelo de avion
     * @param id identificador del modelo de avion
     * @param model inyectado
     * @return vista para eliminar un modelo de avion
     */
    @RequestMapping(value = "/eliminar", method = RequestMethod.GET)
    public String eliminar(@RequestParam("id") Integer id, Model model){
        Modelo modelo = modeloService.findById(id);
        model.addAttribute("modelo", modelo);
        return "eliminarModelo";
    }
    
    /**
     * Elimina un modelo de avion
     * @param modelo a eliminar
     * @param result resultaod de validacion
     * @return listado de modelos de avion
     */
    @RequestMapping(value = "/eliminar", method = RequestMethod.POST)
    public String eliminar(@Valid @ModelAttribute("modelo") Modelo modelo, BindingResult result){
        if(result.hasErrors() || modelo.getId() == null){
            return "eliminarModelo";
        }
        modeloService.delete(modelo);
        return "redirect:listar.html";
    }
    
    /**
     * Busca modelos de avion de acuerdo al nombre dado como parámetro de busqueda
     * @param nombre parametro de busqueda
     * @return lista de modelos de avion
     */
    @RequestMapping(value = "/buscarPorNombre", method = RequestMethod.POST)
    public @ResponseBody List<Modelo> buscarPorNombre(@RequestParam("nombre") String nombre){
        List<Modelo> modelos = modeloService.findByNombreLike(nombre);
        return modelos;
    }
    
}
